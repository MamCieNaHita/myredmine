'use strict';

// simple express server
var express = require('express');
var app = express();
var router = express.Router();

app.use(express.static('public'));
app.get('/test/', function(req, res) {
    res.send('test');
});

app.listen(5000);