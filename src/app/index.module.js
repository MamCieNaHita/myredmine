(function() {
  'use strict';

  /**
   * Main module of the Fuse
   */
  angular
    .module('fuse', [

      // Core
      'app.core',

      // Navigation
      'app.navigation',

      // Toolbar
      'app.toolbar',

      // Quick panel
      'app.quick-panel',

      // Sample
      'app.sample',
      'app.projects',
      'app.features',
      'app.ta_sks',
      'app.additionals',
      'app.custom-directives',
      'app.custom-services',
      'app.auth'
    ]);
  angular.module('mypost');
})();
