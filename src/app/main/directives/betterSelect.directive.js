(function() {
  'use strict';

  angular
    .module('app.custom-directives')
    .directive('betterSelect', betterSelect);
  /*jshint multistr: true */
  /** @ngInject */
  function betterSelect($document, msApi, checkRegex) {
    return {
      restrict: 'E',
      scope: {
        type: '=type',
        task: '=task',
        ngDisabled: '=ngDisabled'
      },
      controller: ['$scope', function($scope) {
        $scope.list = {};
        $scope.typeid = $scope.type + 'id';
        $scope.typename = $scope.type + 'name';
        $scope.namelist = [];
        $scope.phrase = '';
        if ($scope.task[$scope.typename] !== '') {
          $scope.namelist = [{
            id: $scope.task[$scope.typeid],
            name: $scope.task[$scope.typename]
          }];
        }
        $scope.getOnesString = function() {
          if (angular.isArray($scope.namelist)) {
            var string = $scope.namelist.find(function(x) {
              return x.id === $scope.task[$scope.typeid];
            });
            if (string) {
              $scope.task[$scope.typename] = string.name;
              return string.name;
            }
          }
          return '';
        };
        $scope.onMoreClick = function(event) {
          event.stopPropagation();
        };
        $scope.onSearchChange = function(event) {
          event.stopPropagation();
        };
        $scope.templist = [];
        $scope.onOpenProjects = function() {
          if (angular.isArray($scope.namelist)) {
            $scope.templist = $scope.namelist.slice(0);
          }
          $scope.searchOnesPhrase();
        };
        $scope.onCloseProjects = function() {
          if ($scope.task[$scope.typeid]) {
            if ($scope.templist[0] === undefined) {
              $scope.namelist = [$scope.namelist.find(function(x) {
                return x.id === $scope.task[$scope.typeid];
              })];
            } else {
              if ($scope.task[$scope.typeid] === $scope.templist[0].id) {
                $scope.namelist = $scope.templist;
              } else {
                $scope.namelist = [$scope.namelist.find(function(x) {
                  return x.id === $scope.task[$scope.typeid];
                })];
              }
            }
          }
        };
        $scope.searchOnesPhrase = function() {
          $scope.list.page = 0;
          $scope.searchForOnes();
        };
        $scope.searchForOnes = function() {
          $scope.phrase = checkRegex.check($scope.phrase);
          msApi.request($scope.type + '.namelist.new@query', {
            substring: $scope.phrase,
            page: $scope.list.page
          }).then(function(data) {
            if (data.length < 1) {
              $scope.list.total = 0;
              $scope.namelist = {};
            } else {
              $scope.namelist = data.slice(0);
              $scope.list.total = data[0].amount;
            }
          });
        };
        $scope.onesPerPage = 4;
        $scope.oneLeft = function(event) {
          event.stopPropagation();
          if ($scope.list.page) {
            $scope.list.page--;
            $scope.searchForOnes();
          }
        };
        $scope.oneRight = function(event) {
          event.stopPropagation();
          if (($scope.list.page + 1) * $scope.onesPerPage < $scope.list.total) {
            $scope.list.page++;
            $scope.searchForOnes();
          }
        };
      }],
      template: ' <md-select  name="{{type.substr(0,1).toUpperCase()+type.substring(1)}}" class="myselect" ng-model="task[typeid]" md-on-open="onOpenProjects()" md-on-close="onCloseProjects(); func()" style="min-width: 200px;" aria-label="searchProjects" ng-required="!task[typeid]" ng-change="func()" ng-disabled="ngDisabled" md-selected-text="getOnesString()">\
                 <div layout="row" layout-align="center center">\
                     <md-option ng-click="oneLeft($event)" layout-align="center center"><<br />\
                     <span ng-if="list.page>0" style="font-size: x-small">\
                     +{{list.page*onesPerPage}}\
                 </span></md-option>\
                     <md-select-header>\
                         <input type="text" ng-keydown="onSearchChange($event)" ng-change="searchOnesPhrase()" ng-model="phrase" aria-label="Project" style="max-width: 100px"/>\
                     </md-select-header>\
                <md-option ng-click="oneRight($event)" layout-align="center center">><br />\
                         <span ng-if="(list.page+1)*onesPerPage<list.total" style="font-size: x-small">\
                          +{{list.total-(list.page+1)*onesPerPage}}\
                         </span></md-option>\
                 </div>\
                     <md-option ng-repeat="typename in namelist" ng-value="typename.id">{{typename.name}}</md-option>\
             </md-select>'
    };
  }
})();
