(function() {
  'use strict';

  angular
    .module('app.custom-directives')
    .directive('coolUserCheckbox', coolUserCheckbox);
  /*jshint multistr: true */
  /** @ngInject */
  function coolUserCheckbox($document, $timeout, msApi) {
    return {
      restrict: 'E',
      scope: {
        user: '=user',
        level: '=level'
      },
      controller: ['$scope', function($scope) {
        $scope.isWaiting=false;
        $scope.changeLevel = function(){
          $scope.isWaiting=true;
          var index=$scope.user.lvl.indexOf($scope.level);
          var newLevels=$scope.user.lvl.slice(0);
            if(index===-1){
              newLevels.push($scope.level);
            }else{
              newLevels.splice(index,1);
            }
            msApi.request('user.changelevel@save', {
              id: $scope.user.id,
              levels: newLevels
            }).then(function(response){
              if(response.success===true){
                $scope.user.lvl=newLevels;
                $timeout(function(){$scope.isWaiting=false},1000);
              }
            })
        }
      }],
      template: '<md-checkbox ng-if="!isWaiting" ng-checked="user.lvl.indexOf(level)!==-1" aria-label="user-checkBox" ng-click="changeLevel()"></md-checkbox>\
                  <md-progress-circular md-diameter="20" md=mode="indeterminate" ng-if="isWaiting" class="md-accent"></md-progress-circular>'
    };
  }
})();
