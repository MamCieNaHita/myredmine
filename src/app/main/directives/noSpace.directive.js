(function ()
{
    'use strict';

    angular
        .module('app.custom-directives')
        .directive('checkRegex', checkRegex);

    /** @ngInject */
    function checkRegex($document)
    {
        return {
            restrict   : 'A',
            require: 'ngModel',
            link       : function (scope, iElement, attrs, ngModel)
            {
                scope.$watch(attrs.ngModel, function(value){
                    if (!!value)
                    {
                        var temp=scope;
                        var keys=attrs.ngModel.split('.');
                        for(var i=0; i<keys.length-1; i++)
                        {
                            temp=temp[keys[i]];
                        }
                        temp[keys[keys.length-1]]=value.match(/[A-Za-z0-9%_ żźćńółęąśŻŹĆĄŚĘŁÓŃ]*/g).join('');
                    }
                });
            }
        };
    }
})();
