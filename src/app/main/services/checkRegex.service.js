(function() {
  'use strict';

  angular
    .module('app.custom-services')
    .service('checkRegex', checkRegex);

  /** @ngInject */
  function checkRegex() {
    this.check = function(myString) {
      if (!!myString) {
        return myString.match(/[A-Za-z0-9%_ żźćńółęąśŻŹĆĄŚĘŁÓŃ]*/g).join('');
      } else {
        return myString;
      }
  };
  }
})();
