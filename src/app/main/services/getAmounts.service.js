(function() {
  'use strict';

  angular
    .module('app.custom-services')
    .service('GetAmounts', GetAmounts);

  /** @ngInject */
  function GetAmounts($http, msNavigationService, AuthService) {
    this.get = function() {
      $http({
        url: 'http://localhost:5000/getallamounts',
        method: 'GET'
      }).then(function(response) {
        if (!!response.data.project) {
          msNavigationService.saveItem('projects', {
            badge: {
              content: response.data.project,
              color: getRandomColor()
            }
          })
        }
        if(!!response.data.feature) {
          msNavigationService.saveItem('features', {
            badge: {
              content: response.data.feature,
              color: getRandomColor()
            }
          })
        }
        if(!!response.data.task) {
          msNavigationService.saveItem('ta_sks', {
            badge: {
              content: response.data.task,
              color: getRandomColor()
            }
          })
        }
      })
    msNavigationService.saveItem('additionals', {
      hidden: function(){
          return AuthService.loggedUser.lvl.indexOf('admin')===-1;
      }
    });
    }
    function getRandomColor(){
      var tab=[Math.random(),Math.random(),Math.random()];
      var choose1=Math.floor(Math.random()*3);
      tab[choose1]=tab[choose1]*5;
      var sum=tab[0]+tab[1]+tab[2];
      tab=tab.map(function(num){
        return Math.floor(num/sum*255);
      })
      var test='#'+tab.map(function(num){
        var hex =  num.toString(16);
        if(hex.length<2)
        {
          hex='0'+hex;
        }
        return hex;
      }).join('');
      return test;
    }
  }
})();
