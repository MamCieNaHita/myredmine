(function() {
  'use strict';

  angular
    .module('app.auth')
    .service('AuthService', AuthService);

  /** @ngInject */
  function AuthService(msApi, $http, $q, $mdToast, $mdDialog, $timeout) {
    var service = this;
    this.login = login;
    this.register = register;
    this.logout = logout;
    this.getLoggedUser = getLoggedUser;
    this.loggedUser = null;
    this.isLogged = function() {
      return !!service.loggedUser;
    };
    this.checkRole={
      isUser: isUser,
      isAdmin: isAdmin
    };

    function login(email, password) {
      return msApi.request('authlogin@save', {
        username: email,
        password: password
      }).then(function(response) {
        console.log(response);
        if (response.success===false) {
          service.loggedUser=null;
            $mdDialog.show({
              template: '<div style="width: 300px; height: 50px; display: flex; align-items: center; justify-content: center; background-color: #039BE5; color: white; font-weight: bold; font-family: sans;">Wrong email/password!</div>'
            });
            $timeout(function(){$mdDialog.hide()}, 3000);
        }
        else{
          service.loggedUser = response;
        }
      });
    }
    // vertical-align: middle; line-height: 50px;

    function register(name, email, password) {
      return msApi.request('authregister@save', {
        name: name,
        email: email,
        password: password
      });

    }

    function logout() {
      return $http({
        url: 'http://localhost:5000/auth/logout',
        method: 'GET'
      }).then(function(data) {
        service.loggedUser = null;
        return true;
      })

    }

    function getLoggedUser() {
      return $http({
        url: 'http://localhost:5000/auth/getuser',
        method: 'GET'
      }).then(function(response) {
        var data = response.data;
        if (data.success) {
          service.loggedUser = data.user;
          return data.user;
        } else {
          return $q.reject();
        }
      });
    }

    function isUser() {
      if(!!service.loggedUser){
        return service.loggedUser.lvl.indexOf('user') !== -1;
      }
      return false;
    }

    function isAdmin() {
      if(!!service.loggedUser){
        return service.loggedUser.lvl.indexOf('admin') !== -1;
      }
      return false;
    }
}

})();
