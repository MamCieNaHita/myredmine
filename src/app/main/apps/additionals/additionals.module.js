(function ()
{
    'use strict';

    angular
        .module('app.additionals', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.additionals', {
            url      : '/additionals',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/apps/additionals/additionals.html',
                    controller : 'AdditionalsController as vm'
                }
            },
            cache: false,
            resolve  : {
                /*AdditionalPageList: function(msApi) {
                    return msApi.resolve('additional.list.page@get').amount;
                }*/
            },
            bodyClass: 'additionals'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/additionals');

        // Api
        var serverAddress='http://localhost:5000';
        msApiProvider.register('additionalid', ['http://localhost:5000/additional/:id', {id: '@id'}]);
        msApiProvider.register('additional', ['http://localhost:5000/additional/:id', { id: '@id'},{
            update : { method: 'PUT'},
            create : { method: 'POST', url : ['http://localhost:5000/additional/']},
            query : { method: 'GET', url : ['http://localhost:5000/additional/'], isArray:true}}]);
            msApiProvider.register('additionals', [serverAddress + '/additionals/']);
            msApiProvider.register('additionalsup', [serverAddress + '/additionals/:id', {id: '@id'}, {update: {method: 'PUT'}}]);
        msApiProvider.register('additional.list', ['http://localhost:5000/additionallist/:substring/:page'], {substring: '@substring', page: '@page'});
        msApiProvider.register('additional.list.page', [serverAddress+'/additionalpagelist/']);
        msApiProvider.register('featuresconnected', [serverAddress+'/featureconnected/:id'], {id: '@id'}, { query: {method:'GET', isArray:true}});
        msApiProvider.register('tasksconnectedadditional', [serverAddress+'/taskadditional/:id'], {id: '@id'}, { query: {method:'GET', isArray:true}});
        msApiProvider.register('universal.list', [serverAddress+'/universallist',{}, {
            mysave: { method: 'POST', isArray:true}}]
        );
        msApiProvider.register('user.namelist.new', [serverAddress+ '/usernewlist/:substring/:page'], {substring: '@substring', page: '@page'});
        msApiProvider.register('user.getlist', [serverAddress+ '/getusers'], {query: {method:'GET', isArray:true}});
        msApiProvider.register('user.changelevel', [serverAddress+ '/changelevel']);

        // Navigation
        // Navigation
        msNavigationServiceProvider.saveItem('additionals', {
            title : 'Additionals',
            icon  : 'icon-clipboard',
            state : 'app.additionals',
            weight: 9
            // badge: {
            //   content: 5,
            //   color: '#FF6F00'
            // },
        });
    }

})();
