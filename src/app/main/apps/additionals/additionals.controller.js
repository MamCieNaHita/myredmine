(function() {
  'use strict';

  angular
    .module('app.additionals')
    .controller('AdditionalsController', AdditionalsController);

  /** @ngInject */
  function AdditionalsController($scope, $document, $mdDialog, $mdSidenav, msApi) {
    var vm = this;
    vm.taskFilters = {
      search: ''
    };
    vm.page = {
      current: 0
    };
    vm.editedId = 0;
    vm.editedLabelNow = '';
    vm.editedLabelEarlier = '';
    vm.editedType = '';
    vm.addingLabel = '';
    vm.clearOutFocus = function() {
      vm.editedId = 0;
      vm.editedLabelNow = '';
      vm.editedLabelEarlier = '';
    };
    vm.additionals = [];
    vm.makelist = function() {
      msApi.request('additionals@query', {
        method: 'GET'
      }).then(function(data) {
        vm.additionals = data;
        vm.additionals.forEach(function(data) {
          data.data.forEach(function(additional) {
            {
              additional.isEditing = false;
            }
          });
        });
      });
      msApi.request('user.getlist@query').then(function(users){
        vm.userList=users;
      })
    };
    vm.lvlArray=['user','admin'];
    vm.changeEditing = function(additional) {
      additional.isEditing = !additional.isEditing;
    };
    vm.newGoToEdit = function(data) {
      data.isEditing = !data.isEditing;
      data.newLabel = data.label;
    };
    vm.newMakeEdit = function(data, additional) {
      msApi.request('additionalsup@update', {
        id: data.idx,
        type: additional.type,
        data: data.newLabel,
        label: additional.label
      }).then(function(response) {
        data.isEditing = !data.isEditing;
        if(response.response==='ok'){
            data.label = data.newLabel;
        }
      });
    };
    vm.cancelEdit = function(data) {
      data.isEditing = !data.isEditing;
    };
    vm.search = function() {
      vm.page.current = 0;
      vm.makelist();
    };
    vm.makelist();
    vm.deleteOption = function(id, label) {
      var confirm = $mdDialog.confirm()
        .title('Do you want to delete ' + label + '?')
        .ariaLabel('Delete Additional')
        .ok('Delete')
        .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        msApi.request('additionalsup@delete', {
          id: id
        }).then(function(data) {
          vm.makelist();
        });
      }, function() {});
    };
    vm.createOption = function(type, label, data) {
      msApi.request('additionals@save', {
        type: type,
        label: label,
        data: data
      }).then(function(data) {
        vm.makelist();
      });
    };
    vm.changeLevel = function(user, level){
      var index=user.lvl.indexOf(level);
      var newLevels=user.lvl.slice(0);
        if(index===-1){
          newLevels.push(level);
        }else{
          newLevels.splice(index,1);
        }
        msApi.request('user.changelevel@save', {
          id: user.id,
          levels: newLevels
        }).then(function(response){
          if(response.success===true){
            user.lvl=newLevels;
          }
        })
    }
    vm.msScrollOptions = {
      suppressScrollX: true
    };
    // Methods
    vm.preventDefault = preventDefault;
    vm.openAdditionalDialog = openAdditionalDialog;
    init();
    //////////
    /**
     * Initialize the controller
     */
    function init() {

    }

    /**
     * Prevent default
     *
     * @param e
     */
    function preventDefault(e) {
      e.preventDefault();
      e.stopPropagation();
    }

    /**
     * Open new task dialog
     *
     * @param ev
     * @param task
     */
    function openAdditionalDialog(ev, myadditional, task) {
      $mdDialog.show({
        controller: 'AdditionalDialogController',
        controllerAs: 'vm',
        templateUrl: 'app/main/apps/additionals/dialogs/task/task-dialog.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          Task: myadditional,
          event: ev,
          makeList: vm.makelist
        },
        resolve: {
          FeaturesConnected: function(msApi) {
            var newid;
            if (myadditional) {
              newid = myadditional.id;
            } else {
              newid = 0;
            }
            return msApi.resolve('featuresconnected@query', {
              id: newid
            });
          },
          TasksConnected: function(msApi) {
            var newid;
            if (myadditional) {
              newid = myadditional.id;
            } else {
              newid = 0;
            }
            return msApi.resolve('tasksconnectedadditional@query', {
              id: newid
            });
          }
        }
      });
    }


  }
})();
