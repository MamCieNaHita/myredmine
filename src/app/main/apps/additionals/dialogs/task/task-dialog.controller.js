(function() {
  'use strict';

  angular
    .module('app.additionals')
    .controller('AdditionalDialogController', AdditionalDialogController);

  /** @ngInject */
  function AdditionalDialogController($mdDialog, Task, event, makeList, msApi, $scope, $timeout, FeaturesConnected, TasksConnected) {
    var vm = this;
    vm.title = 'Edit Additional';
    vm.task = angular.copy(Task);

    if (!vm.task) {
      vm.task = {
        'name': '',
        'identifier': '',
        'description': ''
      };
      vm.title = 'New Additional';
      vm.newTask = true;
      vm.task.tags = [];
    }
    else {
            vm.newTask = false;
    }
    vm.addNewTask = addNewTask;
    vm.saveTask = saveTask;
    vm.deleteTask = deleteTask;
    vm.closeDialog = closeDialog;
    //////////
    function addNewTask() {
      msApi.request('additional@save', {
        name: vm.task.name,
        identifier: vm.task.identifier,
        description: vm.task.description
      }).then(makeList);
      closeDialog();
    }
    function saveTask() {
      msApi.request('additional@update', {
        id: vm.task.id,
        name: vm.task.name,
        identifier: vm.task.identifier,
        description: vm.task.description
      }).then(makeList);
      closeDialog();
    }
    function deleteTask() {
        var FeatureSubjects=[];
        var FeatureIds=[];
        var TaskSubjects=[];
        var TaskIds=[];
        for(var i=0; i<FeaturesConnected.length; i++)
        {
            FeatureSubjects.push(FeaturesConnected[i].subject);
            FeatureIds.push(FeaturesConnected[i].idf);
        }
        for(var j=0; j<TasksConnected.length; j++)
        {
            TaskSubjects.push(TasksConnected[j].subject);
            TaskIds.push(TasksConnected[j].idt);
        }
        var dependencies='';
        dependencies+='Following subjects will be deleted:';
        for(var k in FeatureSubjects)
        {
            dependencies+='<br />'+FeatureSubjects[k];
        }
        dependencies+='<br />And following tasks:'
        for(var l in TaskSubjects)
        {
            dependencies+='<br />'+TaskSubjects[l];
        }

      if (vm.task.name === '') {
        closeDialog();
        return;
      } else {
        var confirm = $mdDialog.confirm()
          .title('Are you sure?')
          //.content(dependencies)
          .htmlContent(dependencies)
          .ariaLabel('Delete Additional')
          .ok('Delete')
          .cancel('Cancel')
          .targetEvent(event);
        $mdDialog.show(confirm).then(function() {
            for(var m in TaskIds){
                msApi.request('ta_skid@delete', {
                    id:TaskIds[m]
                });
            }
            for(var n in FeatureIds){
                msApi.request('featureid@delete', {
                    id:FeatureIds[n]
                });
            }
          msApi.request('additionalid@delete', {
            id: vm.task.id
          }).then(makeList);
        }, function() {
          // Cancel Action
        });
      }
    }
    function closeDialog() {
      $mdDialog.hide();
    }
  }
})();
