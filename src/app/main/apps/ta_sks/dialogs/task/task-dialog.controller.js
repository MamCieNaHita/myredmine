(function() {
  'use strict';

  angular
    .module('app.ta_sks')
    .controller('Ta_skDialogController', Ta_skDialogController);

  /** @ngInject */
  function Ta_skDialogController($mdDialog, Task, event, makeList, msApi, $scope, $timeout, $filter, checkRegex, TaskDialogOptions, AuthService, GetAmounts) {
    var vm = this;
    vm.loggedUser=AuthService.loggedUser;
    vm.checkRole=AuthService.checkRole;

    vm.ifEditable=function(){
        return ((vm.checkRole.isAdmin())||vm.task.userid===vm.loggedUser.id);
    }
    vm.title = 'Edit Task';
    vm.task = angular.copy(Task);
    vm.newTask = false;
    if (!vm.task) {
      vm.task = {
        'subject': '',
        'description': '',
        'status': '',
        'priority': '',
        'category': '',
        'featurename': '',
        'username': vm.loggedUser.name,
        'userid': vm.loggedUser.id,
        'tracker': '',
        'startDate': new Date(),
        'dueDate': new Date(),
        'doneRatio': 0,
        'estimatedHours': 0
      };
      vm.title = 'New Task';
      vm.newTask = true;
      vm.task.tags = [];
    }
    vm.optionslist = TaskDialogOptions;
    vm.featuresubjectlist = [{
      id: vm.task.featureid,
      subject: vm.task.featurename
    }];
    vm.usernamelist = [{
      id: vm.task.userid,
      name: vm.task.username
    }];
    vm.addNewTask = addNewTask;
    vm.saveTask = saveTask;
    vm.deleteTask = deleteTask;
    vm.closeDialog = closeDialog;

    vm.onSearchChange = function(event) {
      event.stopPropagation();
    };
    vm.searchFeatures = function() {
      vm.featurephrase =
        checkRegex.check(vm.featurephrase);
      msApi.request('feature.subjectlist@query', {
          substring: vm.featurephrase
        },
        function(response) {
          vm.featuresubjectlist = response;
        });
    };
    vm.searchUsers = function() {
      vm.userphrase = checkRegex.check(vm.userphrase);
      msApi.request('user.namelist@query', {
          substring: vm.userphrase
        },
        function(response) {
          vm.usernamelist = response;
        });
    };
    vm.checkRatio =
      function() {
        if (vm.task.doneRatio < 0) {
          vm.task.doneRatio = 0;
        }
        if (vm.task.doneRatio > 100) {
          vm.task.doneRatio = 100;
        }
      };
    //////////
    function addNewTask() {
      msApi.request('ta_sk@save', {
        subject: vm.task.subject,
        description: vm.task.description,
        status: vm.task.status,
        priority: vm.task.priority,
        category: vm.task.category,
        user: vm.task.userid,
        tracker: vm.task.tracker,
        feature: vm.task.featureid,
        estimatedHours: vm.task.estimatedHours,
        startDate: vm.task.startDate.getTime(),
        dueDate: vm.task.dueDate.getTime(),
        doneRatio: vm.task.doneRatio
      }).then(makeList);
      closeDialog();
    }

    function saveTask() {
      msApi.request('ta_sk@update', {
        id: vm.task.id,
        subject: vm.task.subject,
        description: vm.task.description,
        status: vm.task.status,
        priority: vm.task.priority,
        category: vm.task.category,
        user: vm.task.userid,
        tracker: vm.task.tracker,
        feature: vm.task.featureid,
        estimatedHours: vm.task.estimatedHours,
        startDate: vm.task.startDate.getTime(),
        dueDate: vm.task.dueDate.getTime(),
        doneRatio: vm.task.doneRatio
      }).then(makeList);
      closeDialog();
    }

    function deleteTask() {
      if (vm.task.subject === '') {
        closeDialog();
        return;
      } else {
        var confirm = $mdDialog.confirm()
          .title('Are you sure?')
          .content('The Task will be deleted.')
          .ariaLabel('Delete Task')
          .ok('Delete')
          .cancel('Cancel')
          .targetEvent(event);
        $mdDialog.show(confirm).then(function() {
          msApi.request('ta_skid@delete', {
            id: vm.task.id
          }).then(makeList);
        }, function() {
          // Cancel Action
        });
      }
    }

    function closeDialog() {
      $mdDialog.hide();
      GetAmounts();
    }
  }
})();
