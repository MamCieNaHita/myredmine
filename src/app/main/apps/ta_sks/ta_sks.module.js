(function ()
{
    'use strict';

    angular
        .module('app.ta_sks', [])
        .config(config);
    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider, $mdDateLocaleProvider)
    {
        // State
        $stateProvider.state('app.ta_sks', {
            url      : '/tasks',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/apps/ta_sks/ta_sks.html',
                    controller : 'Ta_sksController as vm'
                }
            },
            cache: false,
            resolve  : {
                taskOptions: function(msApi) {
                    return msApi.resolve('task.query.list@query');
                }
            },
            bodyClass: 'ta_sks'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/ta_sks');

        // Api
        var serverAddress='http://localhost:5000';
        msApiProvider.register('ta_skid', ['http://localhost:5000/task/:id', {id: '@id'}]);
        msApiProvider.register('ta_sk', ['http://localhost:5000/task/:id', { id: '@id'},{
            update : { method: 'PUT'},
            create : { method: 'POST', url : ['http://localhost:5000/task/']},
            query : { method: 'GET', url : ['http://localhost:5000/task/'], isArray:true}}]);
        msApiProvider.register('ta_sk.list', ['http://localhost:5000/tasklist/:substring/:page'], {substring: '@substring', page: '@page'});
        msApiProvider.register('ta_sk.list.page', [serverAddress+'/taskpagelist/']);
        msApiProvider.register('ta_sk.options.list', [serverAddress+ '/taskoptions/', {query: {method:'GET'}, isArray:true}]);
        msApiProvider.register('feature.subjectlist', [serverAddress + '/featuresubjectlist/:substring', {substring: '@substring', query: {method: 'GET', isArray: true}}]);
        msApiProvider.register('user.namelist', [serverAddress + '/usernamelist/:substring', {substring: '@substring', query: {method: 'GET', isArray: true}}]);
        msApiProvider.register('task.query.list', [serverAddress+'/querylist2/task']);
        msApiProvider.register('feature.namelist.new', [serverAddress+ '/featurenewlist/:substring/:page'], {substring: '@substring', page: '@page'});


        // Navigation
        msNavigationServiceProvider.saveItem('ta_sks', {
            title : 'Tasks',
            icon  : 'icon-clipboard',
            state : 'app.ta_sks',
            weight: 9
        });
        $mdDateLocaleProvider.formatDate = function(date) {
          return moment(date).format('DD/MM/YYYY');
        };
    }

})();
