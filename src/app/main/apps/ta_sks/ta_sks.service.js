(function() {
  'use strict';

  angular
    .module('app.ta_sks')
    .service('taskService', taskService);

  /** @ngInject */
  function taskService(msApi, $q, checkRegex) {
    this.getList = function(taskFilters, optionslist, optionschosen, page, orderChosen, orderDesc) {
      return $q(function(success, reject) {
        taskFilters.search = checkRegex.check(taskFilters.search);
        var mytab = [];
        optionslist.forEach(function(option,index) {
            mytab.push({
                label: option.label.toLowerCase(),
                name: option.name,
                tab: optionschosen[option.name].slice(0),
                type: option.type
            });
        });
        msApi.request('universal.list@mysave', {
          page: page.current,
          substring: taskFilters.search,
          type: 'task',
          tab: mytab,
          order: orderChosen,
          desc: orderDesc
        }).then(function(response) {
                if (response[0] !== undefined) {
                  page.total = Math.ceil(response[0].amount / 5);
                } else {
                  page.total = 1;
                }
                var mytasks = [];
                mytasks=response;
                mytasks.forEach(function(element){
                    for(var key in element){
                        if(/.*Date/.test(key)){
                            element[key]=new Date(element[key]);
                        }
                    }
                });
          success(mytasks);
        });
    });
    };
    this.getOrderTab = function() {
        return [
            'Subject',
            'Description',
            'Estimated Hours',
            'Done Ratio',
            'Start Date',
            'Due Date',
            'Status',
            'Priority',
            'Tracker',
            'User'
        ];
    };
  }
})();
