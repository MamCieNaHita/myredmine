(function() {
  'use strict';

  angular
    .module('app.ta_sks')
    .controller('Ta_sksController', Ta_sksController);

  /** @ngInject */
  function Ta_sksController($scope, $document, $mdDialog, $mdSidenav, msApi, taskService, taskOptions, GetAmounts, AuthService) {
    var vm = this;
    GetAmounts.get();

    vm.loggedUser=AuthService.loggedUser;
    vm.checkRole=AuthService.checkRole;
    vm.getStyle=function(userid){
      if(userid===vm.loggedUser.id)
      {
      return 'background-color: #E3F2FD;'
      }
      else {
        return ''
      }
    }
    vm.taskFilters = {
      search: ''
    };
    vm.page = {
      current: 0
    };
    vm.optionlistbool = false;
    vm.makelist = function() {
      taskService.getList(vm.taskFilters, vm.optionslist, vm.optionschosen, vm.page, vm.orderChosen, vm.orderDesc).then(function(data) {
        vm.mytasks = data;
      });
    };
    vm.optionAction = function(option, name) {
      if (vm.optionschosen[name].indexOf(option) !== -1) {
        vm.optionschosen[name].splice(vm.optionschosen[name].indexOf(option), 1);
      } else {
        vm.optionschosen[name].push(option);
      }
    };
    vm.orderChosen = 'Subject';
    vm.orderTab = taskService.getOrderTab();
    vm.orderDesc = false;
    vm.fillOptions = function(key, list) {
      vm.optionschosen[key] = list.slice(0);
    };
    vm.optionschosen = {};
    vm.optionslist = taskOptions;
    vm.optionslist.forEach(function(element) {
      vm.optionschosen[element.name] = element.tab.slice(0);
    });
    vm.optionlistbool = true;
    vm.makelist();
    vm.search = function() {
      if (vm.optionlistbool) {
        vm.page.current = 0;
        vm.makelist();
      }
    };
    vm.nextPage = function() {
      if (vm.page.current + 1 < vm.page.total) {
        vm.page.current++;
        vm.makelist();
      }
    };
    vm.showCategories = function(categories) {
      if (!!categories.length) {
        return categories.join(', ');
      } else {
        return '----';
      }
    };
    vm.showDate = function(date) {
        return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
      },
      vm.prevPage = function() {
        if (vm.page.current > 0) {
          vm.page.current--;
          vm.makelist();
        }
      };
    vm.msScrollOptions = {
      suppressScrollX: true
    };
    // Methods
    vm.preventDefault = preventDefault;
    vm.openTa_skDialog = openTa_skDialog;
    init();
    //////////
    /**
     * Initialize the controller
     */
    function init() {

    }

    /**
     * Prevent default
     *
     * @param e
     */
    function preventDefault(e) {
      e.preventDefault();
      e.stopPropagation();
    }

    /**
     * Open new task dialog
     *
     * @param ev
     * @param task
     */
    function openTa_skDialog(ev, myta_sk, task) {
      $mdDialog.show({
        controller: 'Ta_skDialogController',
        controllerAs: 'vm',
        templateUrl: 'app/main/apps/ta_sks/dialogs/task/task-dialog.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          Task: myta_sk,
          event: ev,
          makeList: vm.makelist
        },
        resolve: {
          TaskDialogOptions: function(msApi) {
            return msApi.resolve('ta_sk.options.list@get');
          }
        }
      });
    }
  }
})();
