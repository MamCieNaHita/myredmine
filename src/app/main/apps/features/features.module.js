(function ()
{
    'use strict';

    angular
        .module('app.features', [])
        .config(config);



    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.features', {
            url      : '/features',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/apps/features/features.html',
                    controller : 'FeaturesController as vm'
                }
            },
            cache: false,
            resolve  : {
                featureOptions: function(msApi) {
                    return msApi.resolve('feature.query.list@query');
                }
            },
            bodyClass: 'features'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/features');

        // Api
        var serverAddress='http://localhost:5000';
        msApiProvider.register('featureid', ['http://localhost:5000/feature/:id', {id: '@id'}]);
        msApiProvider.register('feature', ['http://localhost:5000/feature/:id', { id: '@id'},{
            update : { method: 'PUT'},
            create : { method: 'POST', url : ['http://localhost:5000/feature/']},
            query : { method: 'GET', url : ['http://localhost:5000/feature/'], isArray:true}}]);
        msApiProvider.register('feature.list', ['http://localhost:5000/featurelist/:substring/:page'], {substring: '@substring', page: '@page'});
        msApiProvider.register('feature.list.page', [serverAddress+'/featurepagelist/']);
        msApiProvider.register('feature.options.list', [serverAddress+ '/featureoptions/', {query: {method:'GET'}, isArray:true}]);
        msApiProvider.register('project.namelist', [serverAddress + '/projectnamelist/:substring', {substring: '@substring', query: {method: 'GET', isArray: true}}]);
        msApiProvider.register('user.namelist', [serverAddress + '/usernamelist/:substring', {substring: '@substring', query: {method: 'GET', isArray: true}}]);
        msApiProvider.register('tasksconnectedfeature', [serverAddress+'/taskfeature/:id'], {id: '@id'}, { query: {method:'GET', isArray:true}});
        msApiProvider.register('feature.query.list', [serverAddress+'/querylist2/feature']);
    msApiProvider.register('project.namelist.new', [serverAddress+ '/projectnewlist/:substring/:page'], {substring: '@substring', page: '@page'});
        // Navigation
        msNavigationServiceProvider.saveItem('features', {
            title : 'Features',
            icon  : 'icon-clipboard',
            state : 'app.features',
            weight: 9
        });
    }


})();
