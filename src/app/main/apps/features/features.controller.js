(function() {
  'use strict';

  angular
    .module('app.features')
    .controller('FeaturesController', FeaturesController);

  /** @ngInject */
  function FeaturesController($scope, $document, $mdDialog, $mdSidenav, msApi, featureService, featureOptions, GetAmounts, AuthService) {
    var vm = this;
    vm.checkRole=AuthService.checkRole;
    vm.loggedUser=AuthService.loggedUser;

    GetAmounts.get();

    vm.taskFilters = {
      search: ''
    };
    vm.page = {
      current: 0
    };
    vm.getStyle=function(userid){
      if(userid===vm.loggedUser.id)
      {
      return 'background-color: #E3F2FD;'
      }
      else {
        return ''
      }
    }
    vm.makelist = function() {
      featureService.getList(vm.taskFilters, vm.optionslist, vm.optionschosen, vm.page, vm.orderChosen, vm.orderDesc).then(function(data) {
        vm.myfeatures = data;
      });
    };
    vm.optionAction = function(option, name){
        if(vm.optionschosen[name].indexOf(option)!==-1){
            vm.optionschosen[name].splice(vm.optionschosen[name].indexOf(option),1);
        }
        else {
            vm.optionschosen[name].push(option);
        }
    };
    vm.orderChosen='Subject';
    vm.orderTab=featureService.getOrderTab();
    vm.orderDesc=false;
    vm.optionlistbool = false;
    vm.optionschosen = {};
    vm.optionslist = featureOptions;
    vm.optionslist.forEach(function(element) {
      vm.optionschosen[element.name] = element.tab.slice(0);
    });
    vm.optionlistbool = true;
    vm.makelist();
    vm.fillOptions = function(key, list) {
      vm.optionschosen[key] = list.slice(0);
    };
    vm.search = function() {
      if (vm.optionlistbool) {
        vm.page.current = 0;
        vm.makelist();
      }
    };
    vm.showCategories = function(categories) {
      if (!!categories.length) {
        return categories.join(', ');
      } else {
        return '----';
      }
    };
    vm.nextPage = function() {
      if (vm.page.current + 1 < vm.page.total) {
        vm.page.current++;
        vm.makelist();
      }
    };
    vm.prevPage = function() {
      if (vm.page.current > 0) {
        vm.page.current--;
        vm.makelist();
      }
    };
    vm.msScrollOptions = {
      suppressScrollX: true
    };
    // Methods
    vm.preventDefault = preventDefault;
    vm.openFeatureDialog = openFeatureDialog;
    init();
    //////////
    /**
     * Initialize the controller
     */
    function init() {

    }

    /**
     * Prevent default
     *
     * @param e
     */
    function preventDefault(e) {
      e.preventDefault();
      e.stopPropagation();
    }

    /**
     * Open new task dialog
     *
     * @param ev
     * @param task
     */
    function openFeatureDialog(ev, myfeature, task) {
      $mdDialog.show({
        controller: 'FeatureDialogController',
        controllerAs: 'vm',
        templateUrl: 'app/main/apps/features/dialogs/task/task-dialog.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          Task: myfeature,
          event: ev,
          makeList: vm.makelist
        },
        resolve: {
          TasksConnectedFeature: function(msApi) {
            var newid;
            if (myfeature) {
              newid = myfeature.id;
            } else {
              newid = 0;
            }
            return msApi.resolve('tasksconnectedfeature@query', {
              id: newid
            });
          },
          FeatureDialogOptions: function(msApi) {
            return msApi.resolve('feature.options.list@get');
          }
        }
      });
    }
  }
})();
