(function() {
  'use strict';

  angular
    .module('app.features')
    .controller('FeatureDialogController', FeatureDialogController);

  /** @ngInject */
  function FeatureDialogController($mdDialog, Task, event, makeList, msApi, $scope, $timeout, TasksConnectedFeature, checkRegex, FeatureDialogOptions, AuthService, GetAmounts) {
    var vm = this;
    vm.loggedUser=AuthService.loggedUser;
    vm.checkRole=AuthService.checkRole;

    vm.ifEditable=function(){
        return ((vm.checkRole.isAdmin())||vm.task.userid===vm.loggedUser.id);
    }

    vm.title = 'Edit Feature';
    vm.task = angular.copy(Task);

    vm.log = function() {
      console.log(vm.task.status);
      console.log(vm.optionslist.status);
    };
    vm.newTask = false;
    if (!vm.task) {
      vm.task = {
        'subject': '',
        'description': '',
        'status': '',
        'priority': '',
        'category': [],
        'project': '',
        'userid': vm.loggedUser.id,
        'username': vm.loggedUser.name,
        'estimatedHours': 0
      };
      vm.title = 'New Feature';
      vm.newTask = true;
      vm.task.tags = [];
    }
    vm.optionslist=FeatureDialogOptions;
    vm.projectphrase = '';
    vm.projectnamelist = [{
      id: vm.task.projectid,
      name: vm.task.projectname
    }];
    vm.usernamelist = [{
      id: vm.task.userid,
      name: vm.task.username
    }];
    vm.addNewTask = addNewTask;
    vm.saveTask = saveTask;
    vm.deleteTask = deleteTask;
    vm.closeDialog = closeDialog;
    vm.projectlist = {
      page: 0,
      total: 0
    };
    vm.onMoreClick = function(event) {
      event.stopPropagation();
    };
    vm.onSearchChange = function(event) {
      event.stopPropagation();
    };
    vm.searchUsers = function() {
        vm.userphrase=checkRegex.check(vm.userphrase);
      msApi.request('user.namelist@query', {
        substring: vm.userphrase
      }, function(response) {
        vm.usernamelist = response;
      });
    };
    //////////
    function addNewTask() {
      msApi.request('feature@save', {
        subject: vm.task.subject,
        description: vm.task.description,
        status: vm.task.status,
        priority: vm.task.priority,
        category: vm.task.category,
        project: vm.task.projectid,
        user: vm.task.userid,
        estimatedHours: vm.task.estimatedHours
      }).then(makeList);
      closeDialog();
    }
    function saveTask() {
      msApi.request('feature@update', {
        id: vm.task.id,
        subject: vm.task.subject,
        description: vm.task.description,
        status: vm.task.status,
        priority: vm.task.priority,
        category: vm.task.category,
        project: vm.task.projectid,
        user: vm.task.userid,
        estimatedHours: vm.task.estimatedHours
      }).then(makeList);
      closeDialog();
    }
    function deleteTask() {
      var TaskSubjects = [];
      var TaskIds = [];
      for (var j = 0; j < TasksConnectedFeature.length; j++) {
        TaskSubjects.push(TasksConnectedFeature[j].subject);
        TaskIds.push(TasksConnectedFeature[j].idt);
      }
      var dependencies = '';
      dependencies += 'Following subjects will be tasks:';
      TaskSubjects.forEach(function(task) {
          dependencies+='<br />'+task;
      });
      if (vm.task.subject === '') {
        closeDialog();
        return;
      } else {
        var confirm = $mdDialog.confirm()
          .title('Are you sure?')
          .htmlContent(dependencies)
          .ariaLabel('Delete Feature')
          .ok('Delete')
          .cancel('Cancel')
          .targetEvent(event);
        $mdDialog.show(confirm).then(function() {
            TaskIds.forEach(function(id) {
                msApi.request('ta_skid@delete', {
                    id: id
                });
            });
          msApi.request('featureid@delete', {
            id: vm.task.id
          }).then(makeList);
        }, function() {
          // Cancel Action
        });
      }
    }
    function closeDialog() {
      $mdDialog.hide();
      GetAmounts.get();
    }


  }
})();
