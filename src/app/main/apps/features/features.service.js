(function() {
  'use strict';

  angular
    .module('app.features')
    .service('featureService', featureService);

  /** @ngInject */
  function featureService(msApi, $q, checkRegex) {
    this.getList = function(taskFilters, optionslist, optionschosen, page, orderChosen, orderDesc) {
      return $q(function(success, reject) {
        taskFilters.search = checkRegex.check(taskFilters.search);
        var mytab = [];
        optionslist.forEach(function(option, index) {
          mytab.push({
            label: option.label.toLowerCase(),
            name: option.name,
            tab: optionschosen[option.name].slice(0),
            type: option.type
          });
        });
        msApi.request('universal.list@mysave', {
          page: page.current,
          substring: taskFilters.search,
          type: 'feature',
          tab: mytab,
          order: orderChosen,
          desc: orderDesc
        }).then(function(response) {
          if (response[0] !== undefined) {
            page.total = Math.ceil(response[0].amount / 5);
          } else {
            page.total = 1;
          }
          var myfeatures = [];
          myfeatures = response;
          success(myfeatures);
        });
      });
    };
    this.getOrderTab = function() {
      return [
        'Subject',
        'Description',
        'Estimated Hours',
        'Project',
        'Status',
        'Priority',
        'User'
      ];
    };
  }
})();
