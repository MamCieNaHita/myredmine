(function() {
  'use strict';

  angular
    .module('app.projects')
    .service('projectService', projectService);

  /** @ngInject */
  function projectService(msApi, $q, checkRegex, $http) {
    this.getList = function(taskFilters, page) {
      return $q(function(success, reject) {
          taskFilters.search=checkRegex.check(taskFilters.search);
            // msApi.request('project.list@query', {
            //   method: 'GET',
            //   page: page.current,
            //   substring: taskFilters.search
            $http({
                method: 'GET',
                url: 'http://localhost:5000/projectlist/'+taskFilters.search+'/'+page.current
            })
         .then(function(response) {
             response=response.data;
              var myprojects = [];
              if(response[0]!== undefined) {
                  page.total = Math.ceil(response[0].amount / 5);
              } else {
                  page.total = 1;
              }
            myprojects=response;
              success(myprojects);
          });
    });
    };
  }
})();
