(function() {
  'use strict';

  angular
    .module('app.projects')
    .provider('projectsAmount', function() {
      var myProvider=this;
      this.$get = function(){
        return {}
      }
      var http = angular.injector(['ng']).get('$http');
      this.getAmountFun = function(){
        return http({
           url: 'http://localhost:5000/projectamount',
           method: 'GET'
         }).then(function(response){
           console.log(response.data.amount);
             return response.data.amount;
         });
      }
      this.setAmount = function(badge){
        http({
          url: 'http://localhost:5000/projectamount',
          method: 'GET'
        }).then(function(response){
          badge.content = response.data.amount;
        })
      }
    });

  /** @ngInject */

})();
