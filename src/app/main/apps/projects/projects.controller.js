(function() {
  'use strict';

  angular
    .module('app.projects')
    .controller('ProjectsController', ProjectsController);

  /** @ngInject */
  function ProjectsController($scope, $document, $mdDialog, $mdSidenav, msApi, checkRegex, projectService, AuthService, msNavigationService, $http, GetAmounts) {
    GetAmounts.get();
    var vm = this;
    vm.checkRole = AuthService.checkRole;
    console.log(AuthService.loggedUser);
    vm.taskFilters = {
      search: ''
    };
    vm.page = {
      current: 0
    };
    vm.makelist = function() {
      projectService.getList(vm.taskFilters, vm.page).then(function(data) {
        vm.myprojects = data;
      });
    };
    vm.search = function() {
      vm.page.current = 0;
      vm.makelist();
    };
    vm.makelist();
    vm.nextPage = function() {
      if (vm.page.current + 1 < vm.page.total) {
        vm.page.current++;
        vm.makelist();
      }
    };
    vm.prevPage = function() {
      if (vm.page.current > 0) {
        vm.page.current--;
        vm.makelist();
      }
    };
    vm.msScrollOptions = {
      suppressScrollX: true
    };
    // Methods
    vm.preventDefault = preventDefault;
    vm.openProjectDialog = openProjectDialog;
    init();
    //////////
    /**
     * Initialize the controller
     */
    function init() {

    }

    /**
     * Prevent default
     *
     * @param e
     */
    function preventDefault(e) {
      e.preventDefault();
      e.stopPropagation();
    }

    /**
     * Open new task dialog
     *
     * @param ev
     * @param task
     */
    function openProjectDialog(ev, myproject, task) {
      $mdDialog.show({
        controller: 'ProjectDialogController',
        controllerAs: 'vm',
        templateUrl: 'app/main/apps/projects/dialogs/task/task-dialog.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          Task: myproject,
          event: ev,
          makeList: vm.makelist
        },
        resolve: {
          FeaturesConnected: function(msApi) {
            var newid;
            if (myproject) {
              newid = myproject.id;
            } else {
              newid = 0;
            }
            return msApi.resolve('featuresconnected@query', {
              id: newid
            });
          },
          TasksConnected: function(msApi) {
            var newid;
            if (myproject) {
              newid = myproject.id;
            } else {
              newid = 0;
            }
            return msApi.resolve('tasksconnectedproject@query', {
              id: newid
            });
          }
        }
      });
    }
  }
})();
