(function() {
  'use strict';

  angular
    .module('app.projects')
    .controller('ProjectDialogController', ProjectDialogController);

  /** @ngInject */
  function ProjectDialogController($mdDialog, Task, event, makeList, msApi, $scope, $timeout, FeaturesConnected, TasksConnected, AuthService, GetAmounts) {
    var vm = this;
    vm.checkRole=AuthService.checkRole;
    vm.title = 'Edit Project';
    vm.task = angular.copy(Task);

    if (!vm.task) {
      vm.task = {
        'name': '',
        'identifier': '',
        'description': ''
      };
      vm.title = 'New Project';
      vm.newTask = true;
      vm.task.tags = [];
    }
    else {
            vm.newTask = false;
    }
    vm.addNewTask = addNewTask;
    vm.saveTask = saveTask;
    vm.deleteTask = deleteTask;
    vm.closeDialog = closeDialog;
    //////////
    function addNewTask() {
      msApi.request('project@save', {
        name: vm.task.name,
        identifier: vm.task.identifier,
        description: vm.task.description
      }).then(makeList);
      closeDialog();
    }
    function saveTask() {
      msApi.request('project@update', {
        id: vm.task.id,
        name: vm.task.name,
        identifier: vm.task.identifier,
        description: vm.task.description
      }).then(makeList);
      closeDialog();
    }
    function deleteTask() {
        var FeatureSubjects=[];
        var FeatureIds=[];
        var TaskSubjects=[];
        var TaskIds=[];
        for(var i=0; i<FeaturesConnected.length; i++)
        {
            FeatureSubjects.push(FeaturesConnected[i].subject);
            FeatureIds.push(FeaturesConnected[i].idf);
        }
        for(var j=0; j<TasksConnected.length; j++)
        {
            TaskSubjects.push(TasksConnected[j].subject);
            TaskIds.push(TasksConnected[j].idt);
        }
        var dependencies='';
        dependencies+='Following subjects will be deleted:';
        FeatureSubjects.forEach(function(subject) {
            dependencies+='<br />'+subject;
        });
        dependencies+='<br />And following tasks:';
        TaskSubjects.forEach(function(task) {
            dependencies+='<br />'+task;
        });
      if (vm.task.name === '') {
        closeDialog();
        return;
      } else {
        var confirm = $mdDialog.confirm()
          .title('Are you sure?')
          //.content(dependencies)
          .htmlContent(dependencies)
          .ariaLabel('Delete Project')
          .ok('Delete')
          .cancel('Cancel')
          .targetEvent(event);
        $mdDialog.show(confirm).then(function() {
            TaskIds.forEach(function(id) {
                msApi.request('ta_skid@delete', {
                    id: id
                });
            });
            FeatureIds.forEach(function(id) {
                msApi.request('featureid@delete', {
                    id: id
                });
            });
          msApi.request('projectid@delete', {
            id: vm.task.id
          }).then(makeList);
        }, function() {
          // Cancel Action
        });
      }
    }
    function closeDialog() {
      $mdDialog.hide();
      GetAmounts.get();
    }
  }
})();
