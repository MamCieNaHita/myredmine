(function ()
{
    'use strict';

    angular
        .module('app.projects', [])
        .config(config);


    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider, projectsAmountProvider)
    {
        // State
        $stateProvider.state('app.projects', {
            url      : '/projects',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/apps/projects/projects.html',
                    controller : 'ProjectsController as vm'
                }
            },
            cache: false,
            resolve  : {
                /*ProjectPageList: function(msApi) {
                    return msApi.resolve('project.list.page@get').amount;
                }*/
            },
            bodyClass: 'projects'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/projects');

        // Api
        var serverAddress='http://localhost:5000';
        msApiProvider.register('projectid', ['http://localhost:5000/project/:id', {id: '@id'}]);
        msApiProvider.register('project', ['http://localhost:5000/project/:id', { id: '@id'},{
            update : { method: 'PUT'},
            create : { method: 'POST', url : ['http://localhost:5000/project/']},
            query : { method: 'GET', url : ['http://localhost:5000/project/'], isArray:true}}]);
        msApiProvider.register('project.list', ['http://localhost:5000/projectlist/:substring/:page'], {substring: '@substring', page: '@page'});
        msApiProvider.register('project.list.page', [serverAddress+'/projectpagelist/']);
        msApiProvider.register('featuresconnected', [serverAddress+'/featureconnected/:id'], {id: '@id'}, { query: {method:'GET', isArray:true}});
        msApiProvider.register('tasksconnectedproject', [serverAddress+'/taskproject/:id'], {id: '@id'}, { query: {method:'GET', isArray:true}});
        // Navigation
        // Navigation
        var myBadge={
          content: 0,
          color: '#FF6F00'
        }
        msNavigationServiceProvider.saveItem('projects', {
          title : 'Projects',
          icon  : 'icon-clipboard',
          state : 'app.projects',
          weight: 1
        });
    }


})();
