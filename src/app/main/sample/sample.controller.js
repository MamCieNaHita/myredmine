(function ()
{
    'use strict';

    angular
        .module('app.sample')
        .controller('SampleController', SampleController);

    /** @ngInject */
    function SampleController($scope, SampleData, $http, msApi)
    {
        var vm = this;
        vm.num=2;
        // Data
        vm.helloText = SampleData.data.helloText;
        //vm.data = this.query();
        //console.log(this.query());
        //console.log(SampleData);
        //console.log(vm);
        // Methods
       /* $scope.myGet = function(id) {
            if(id === undefined || id === null || id === NaN)
            {
                id = '';
            }
        $http.get('http://localhost:5000/project/'+ id).then(function(response) {
                vm.mydataunf = response.data;
                vm.mydata = '';
                for(var i=0; i<vm.mydataunf.length; i++)
                {
                    vm.mydata+= 'ID: ' + vm.mydataunf[i].m._id + '\n';
                    vm.mydata+= 'TITLE: ' + vm.mydataunf[i].m.properties.title + '\n';
                    vm.mydata+= 'RELEASED: ' + vm.mydataunf[i].m.properties.released + '\n\n';

                }
                console.log(response.data);
            })
    };*/
        $scope.myTest = function() {
            console.log("dupa");
        }

        $scope.myPost = function(title, year) {
            var data = { title: title, year: year };
            $http.post('http://localhost:5000/movie', data);
            console.log(data);
        }
        $scope.myDelete = function(id) {
            $http.delete('http://localhost:5000/movie/' + id);
        }
        $scope.myUpdate = function(id, title, year) {
            var data = { title: title, year: year};
            $http.put('http://localhost:5000/movie/' + id, data);
        }
        $scope.myGetList = function() {
            msApi.request('movie@query', {method:'GET'},
            function(response)
            {
                vm.mydata = response;
            })
        }
        $scope.myNewGet = function(id) {
            msApi.request('movieid@get', {id: id},
                function(response)
                {
                    vm.mydata = response;
                })
        }
        $scope.myNewPost = function(name, identifier, description) {
               msApi.request('project@save', {name: name, identifier: identifier, description: description})
        }
        $scope.myNewDelete = function(id) {
            msApi.request('movie@update', {title: title, year:year})
        }
        vm.mydata='';
        console.log(vm.mydata);
        //////////
    }

})();
