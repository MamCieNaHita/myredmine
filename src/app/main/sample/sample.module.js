(function ()
{
    'use strict';

    angular
        .module('app.sample', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.sample', {
                url    : '/sample',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/sample/sample.html',
                        controller : 'SampleController as vm'
                    }
                },
                resolve: {
                    SampleData: function (msApi)
                    {
                        return msApi.resolve('sample@get');
                    },
               /*MyGetList: function (msApi)
                    {
                        return msApi.resolve('movie@query', {method:'GET'});
                    }*/
                   /* SampleData: function($resource)
                    {
                        console.log("wtf");
                        //return $resource.get('http://localhost:5000/movie/2'); //  vm.data = response;
                        return $resource('http://localhost:5000/movie/2', {} ,{
                            query: {method: 'GET'}
                        })
                    }*/
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/sample');

        // Api
        msApiProvider.register('sample', ['app/data/sample/sample.json']);
        msApiProvider.register('project', ['http://localhost:5000/project/']);
        msApiProvider.register('movie', ['http://localhost:5000/movie/']);
        msApiProvider.register('movieid', ['http://localhost:5000/movie/:id', {id: '@id'}])

        // Navigation
        // msNavigationServiceProvider.saveItem('fuse', {
        //     title : 'SAMPLE',
        //     group : true,
        //     weight: 1
        // });
        //
        // msNavigationServiceProvider.saveItem('fuse.sample', {
        //     title    : 'Sample',
        //     icon     : 'icon-tile-four',
        //     state    : 'app.sample',
        //     /*stateParams: {
        //         'param1': 'page'
        //      },*/
        //     translate: 'SAMPLE.SAMPLE_NAV',
        //     weight   : 1
        // });
    }
})();
