(function() {
  'use strict';

  angular
    .module('fuse')
    .run(runBlock);

  /** @ngInject */
  function runBlock($rootScope, $timeout, $state, AuthService) {
    // Activate loading indicator
    var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
      $rootScope.loadingProgress = true;
      if (toState.name === '') {} else {
        if (!AuthService.isLogged()) {
          if (toState.name !== 'app.auth_login' && toState.name !== 'app.auth_register') {
            event.preventDefault();
          }
          AuthService.getLoggedUser().then(function(data) {
            if (toState.name === 'app.auth_login' || toState.name === 'app.auth_register') {
              $state.go('app.projects');
            } else {
              if(toState.name=== 'app.additionals'){
                if(!AuthService.checkRole.isAdmin()){
                  $state.go('app.projects');
                }
                else{
                  $state.go(toState.name);
                }
              }
              else {
                $state.go(toState.name);
              }
            }
          }).catch(function(err) {
            if (toState.name !== 'app.auth_login' && toState.name !== 'app.auth_register') {
              $state.go('app.auth_login');
            }
          });
        } else {
          if (toState.name === 'app.auth_login') {
            $state.go('app.projects');
          }
          if (toState.name === 'app.additionals') {
            if(!AuthService.checkRole.isAdmin()){
              $state.go('app.projects');
            }
          }
        }
      }
    });

    // De-activate loading indicator
    var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function() {
      $timeout(function() {
        $rootScope.loadingProgress = false;
      });
    });

    // Store state in the root scope for easy access
    $rootScope.state = $state;

    // Cleanup
    $rootScope.$on('$destroy', function() {
      stateChangeStartEvent();
      stateChangeSuccessEvent();
    });
  }
})();
