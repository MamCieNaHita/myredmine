'use strict';
// neo4j
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var graphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new graphDatabase('http://neo4j:neopass@localhost:7474');

module.exports = {
  getProjectNode: getNode,
  deleteProjectNode: deleteNode,
  getListProjectNode: getList,
  createProjectNode: createNode,
  updateProjectNode: updateNode,
  getMatchingList: getMatchingList,
  getFullList: getFullList,
  getPageList: getPageList,
  getProjectNames: getProjectNames,
  getProjectListNames: getProjectListNames,
  getProjectListAmount: getProjectListAmount,
  getProjectAmount: getProjectAmount
};

function getNode(id) {
  var query = [
    'MATCH (m:Project)',
    'WHERE id(m) = {id}',
    'RETURN m'
  ];

  var params = {
    id: Number(id)
  };

  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function createNode(name, identifier, description) {
  var query = ['CREATE (:Project{name:{name}, identifier:{identifier}, description:{description}}) '];

  var params = {
    name: String(name),
    identifier: String(identifier),
    description: String(description)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getList() {
  var query = ['MATCH (m:Project)', 'return m'];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function deleteNode(id) {
  var query = [
    'MATCH (m:Project)',
    'WHERE id(m) = {id}',
    'WITH m',
    'DETACH DELETE m'
  ];
  var params = {
    id: Number(id)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function updateNode(id, name, identifier, description) {
  var query = [
    'MATCH (m:Project)',
    'WHERE id(m) = {id}',
    'WITH m',
    'SET m.name = {name}',
    'SET m.identifier = {identifier}',
    'SET m.description = {description}',
    'RETURN m'
  ];
  var params = {
    id: Number(id),
    name: String(name),
    identifier: String(identifier),
    description: String(description)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getMatchingList(substring, page, pagination) {
  var query = ['MATCH (m:Project),(n:Project)',
    'WHERE m.name=~".*' + substring + '.*" OR m.identifier=~".*' + substring + '.*" OR m.description=~".*' + substring + '.*"',
    'WITH m,n',
    'WHERE n.name=~".*' + substring + '.*" OR n.identifier=~".*' + substring + '.*" OR n.description=~".*' + substring + '.*"',
    'RETURN m, count(n) as pagetotal',
    'SKIP {page}*{pagination}',
    'LIMIT {pagination}'
  ];
  var params = {
    substring: String(substring),
    page: Number(page),
    pagination: Number(pagination)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getFullList(page, pagination) {
  var query = ['MATCH (m:Project),(n:Project)', 'RETURN m, count(n) as pagetotal', 'SKIP {page}*{pagination}', 'LIMIT {pagination}'];
  var params = {
    page: Number(page),
    pagination: Number(pagination)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getPageList() {
  var query = ['MATCH (m:Project)', 'RETURN COUNT(m) AS AMOUNT'];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getProjectNames(substring) {
  var query = ['MATCH (p:Project)',
    'WHERE p.name =~ ".*' + substring + '.*"',
    'RETURN p.name AS name, id(p) AS id'
  ];
  var params = {
    substring: String(substring)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}
function getProjectListQuery(substring) {
    return ['MATCH (p:Project)', 'WHERE p.name contains "' + substring + '"'];
}

function getProjectListNames(substring, page)
{
    var query = getProjectListQuery(substring);
    query.push('RETURN p.name AS name, id(p) AS id','SKIP '+page+'*4','LIMIT 4');
    return neodb.cypherAsync({
      query: query.join('\n')
    });
}
function getProjectListAmount(substring)
{
    var query = getProjectListQuery(substring);
    query.push('RETURN count(p) AS amount');
    return neodb.cypherAsync({
      query: query.join('\n')
    });
}

function getProjectAmount()
{
  var query= ['MATCH (p:Project)', 'RETURN count(p) as amount'];
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}
