'use strict';
//jshint node: true
var projectService = require('./../services/project.service');

module.exports = {
  getProjectNode: getProjectNode,
  getProjectList: getProjectList,
  createProjectNode: createProjectNode,
  deleteProjectNode: deleteProjectNode,
  updateProjectNode: updateProjectNode,
  getMatchingList: getMatchingList,
  getFullList: getFullList,
  getPageList: getPageList,
  getProjectNames: getProjectNames,
  getTestList: getTestList,
  getProjectAmount: getProjectAmount
};

var pagination = 5;

function getProjectNode(req, res, next) {
  var id = req.params[0];
  projectService.getProjectNode(id).then(function(data) {
    res.send(data[0]);
  });
}

function getProjectList(req, res, next) {
  projectService.getListProjectNode().then(function(data) {
    res.send(data);
  });
}

function createProjectNode(req, res, next) {
  if (checkIfValid(req.body)) {
    res.send('sth went wrong');
    return;
  }
  projectService.createProjectNode(req.body.name, req.body.identifier, req.body.description).then(function(data) {
    res.send(data[0]);
  });
}

function updateProjectNode(req, res, next) {
  if (checkIfValid(req.body)) {
    res.send('sth went wrong');
    return;
  }
  var id = req.params[0];
  projectService.updateProjectNode(id, req.body.name, req.body.identifier, req.body.description).then(function(data) {
    res.send(data[0]);
  }).catch(function(err) {
    res.send('error');
  });
}

function deleteProjectNode(req, res, next) {
  var id = req.params[0];
  projectService.deleteProjectNode(id).then(function(data) {
    res.send(data[0]);
  }).catch(function(err) {
    res.send('error');
  });
}



function getMatchingList(req, res, next) {
  var substring = req.params[0];
  var page = req.params[1];
  projectService.getMatchingList(substring, page, pagination).then(function(data) {
    res.send(data);
  });
}

function getFullList(req, res, next) {
  var page = req.params[0];
  projectService.getFullList(page, pagination).then(function(data) {
    res.send(data);
  });
}


function getPageList(req, res, next) {
  projectService.getPageList().then(function(data) {
    res.send({
      amount: Math.ceil((data[0].AMOUNT) / pagination)
    });
  });
}

function getProjectNames(req, res, next) {
  var substring;
  if (req.params[0] === undefined) {
    substring = '';
  } else {
    substring = req.params[0];
  }
  projectService.getProjectNames(substring).then(function(data) {
    res.send(data);
  }).catch(function(err) {
    res.send(err);
  });
}

function checkIfValid(body) {
  if (!!body.name) {
    if (!!body.identifier) {
      if (!!body.description) {
        return false;
      }
    }
  }
  return true;
}

function getTestList(req, res, next) {
  var substring;
  if (req.params[0] === undefined) {
    substring = '';
  } else {
    substring = req.params[0];
  }
  var page = req.params[1];
  if (req.params[1] === undefined) {
    substring = '';
    page = req.params[0];
  }
  projectService.getProjectListNames(substring, page).then(function(data) {
    projectService.getProjectListAmount(substring).then(function(amount) {
      res.send(data.map(function(x) {
        x.amount = amount[0].amount;
        return x;
      }));
    });
  });
}

function getProjectAmount(req, res, next) {
  projectService.getProjectAmount().then(function(amount){
    res.send(amount[0]);
  }).catch(function(err){
      res.json({
        amount: 0,
        success: false,
        error: "Problem with getting project amount"
      });
  })
}
