//jshint node: true
'use strict';

var express = require('express');
var controller = require('./../controllers/project.controller');

var projectRouter = express.Router();

projectRouter.post('/project/', controller.createProjectNode);
// projectRouter.post('/legacy/project/', controller.createProjectNode);
projectRouter.put(/^\/project\/([0-9_]+)$/, controller.updateProjectNode);
projectRouter.delete(/^\/project\/([0-9_]+)$/, controller.deleteProjectNode);
// projectRouter.get(/^\/projectlist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getMatchingList);
projectRouter.get(/^\/legacy\/projectlist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getMatchingList);
// projectRouter.get(/^\/projectlist\/\/(\w+)$/, controller.getFullList);
projectRouter.get(/^\/legacy\/projectlist\/\/(\w+)$/, controller.getFullList);
projectRouter.get('/projectpagelist/', controller.getPageList);
projectRouter.get(/^\/projectnamelist\/([A-Za-z0-9%]+)$/, controller.getProjectNames);
projectRouter.get(/projectnamelist/, controller.getProjectNames);
projectRouter.get(/^\/projectnewlist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getTestList);
projectRouter.get(/^\/projectnewlist\/\/([0-9]+)$/, controller.getTestList);
// projectRouter.get(/^\/projectamount$/, controller.getProjectAmount);

module.exports = projectRouter;
