'use strict';
//neo4j
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var graphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new graphDatabase('http://neo4j:neopass@localhost:7474');
var bodyParser = require('body-parser');

module.exports = {
    checkByValue: checkByValue,
    checkById: checkById
};

function checkByValue(value, type, property) {
    var query=[
        'MATCH (x:' + type + ')',
        'RETURN 1 IN collect(CASE',
        'WHEN {value} IN x.'+ property,
        'THEN 1',
        'ELSE 0 END) as result'
    ];
    var params={
        value: String(value),
        type: String(type),
        property: String(property)
    };
    return neodb.cypherAsync({
      query: query.join('\n'),
      params: params
    });
}

function checkById(checkedId, type)
{
    var query=[
        'MATCH (x:' + type + ')',
        'RETURN 1 IN collect(CASE',
        'WHEN {checkedId} = id(x)',
        'THEN 1',
        'ELSE 0 END) as result'
    ];
    var params={
        checkedId: Number(checkedId),
        type: String(type)
    };
    return neodb.cypherAsync({
      query: query.join('\n'),
      params: params
    });
}
