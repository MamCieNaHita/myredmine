'use strict';
//neo4j
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var graphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new graphDatabase('http://neo4j:neopass@localhost:7474');
var bodyParser = require('body-parser');


module.exports = {
  createFeature: createFeature,
  getStatus: getStatus,
  getPriority: getPriority,
  getCategory: getCategory,
  getMatchingList: getMatchingList,
  getPageList: getPageList,
  deleteFeature: deleteFeature,
  getFeatureById: getFeatureById,
  getFeatureSubjects: getFeatureSubjects,
  addCategories: addCategories,
  getConnectedFeatures: getConnectedFeatures,
  getMatchingListAmount: getMatchingListAmount,
  getFeatureListNames: getFeatureListNames,
  getFeatureListAmount: getFeatureListAmount
};

function createFeature(project, subject, description, status, priority, author, category, estimatedHours) {
  var query = [
    'CREATE (f:Feature{subject:{subject}, description:{description}, estimatedHours:{estimatedHours}}) ',
    'WITH f',
    'MATCH (p:Project),(u:User),(a:Priority),(s:Status)',
    'WHERE id(p)= {project}',
    'WITH p,u,a,s,f',
    'WHERE id(u)= {author}',
    'WITH p,u,a,s,f',
    'WHERE a.priority = {priority}',
    'WITH p,u,a,s,f',
    'WITH p,u,a,s,f',
    'WHERE s.status = {status}',
    'WITH p,u,a,s,f',
    'MERGE (f)-[:Project]->(p)',
    'MERGE (f)-[:Author]->(u)',
    'MERGE (f)-[:Priority]->(a)',
    'MERGE (f)-[:Status]->(s)',
    'RETURN id(f) AS idf'
  ];
  var params = {
    project: Number(project),
    subject: String(subject),
    description: String(description),
    status: String(status),
    priority: String(priority),
    author: Number(author),
    category: category,
    estimatedHours: Number(estimatedHours)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function addCategories(id, category) {
  var makeCat = function() {
    var result = '[';
    for (var i = 0; i < category.length; i++) {
      result += '"' + category[i] + '"';
      if (i + 1 < category.length) {
        result += ',';
      }
    }
    return result + ']';
  };
  var query = [
    'MATCH (f:Feature) WHERE id(f)=' + id,
    'MATCH (c:Category) WHERE c.category IN ' + makeCat(),
    'MERGE (f)-[:Category]->(c)'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getStatus() {
  var query = [
    'MATCH (s:Status)',
    'RETURN s'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getPriority() {
  var query = [
    'MATCH (p:Priority)',
    'RETURN p'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getCategory() {
  var query = [
    'MATCH (c:Category)',
    'RETURN c'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getMatchingList(substring, page, pagination) {
  var query = getMatchingQuery(substring);
  query.push('RETURN f AS feature, p AS project, s AS status, a AS priority, u AS user, collect(c) AS category');
  query.push('ORDER BY id(f)');
  query.push('SKIP {page}*{pagination}');
  query.push('LIMIT {pagination}');
  var params = {
    substring: String(substring),
    page: Number(page),
    pagination: Number(pagination)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getMatchingListAmount(substring, page, pagination) {
  var query = getMatchingQuery(substring);
  query.push('WITH f, collect(c) as col', 'RETURN count(f) as pagetotal'); // + ' OR c.category=~".*' + substring + '.*"',
  var params = {
    substring: String(substring),
    page: Number(page),
    pagination: Number(pagination)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getMatchingQuery(substring) {
  var con = ' CONTAINS "' + substring + '" OR ';
  var mat = 'MATCH (f:Feature)-[]->';
  return [
    mat + '(p:Project)',
    mat + '(s:Status)',
    mat + '(a:Priority)',
    mat + '(u:User)',
    'OPTIONAL ' + mat + '(c:Category)',
    'WITH f,p,s,a,u,c',
    'WHERE f.subject' + con + 'f.description' + con + 'p.name' + con + 'p.description' + con + 'p.identifier' + con + 's.status' + con + 'a.priority' + con + 'u.name CONTAINS "' + substring + '"'
  ];
}

function getPageList() {
  var query = ['MATCH (f:Feature)', 'RETURN COUNT(f) AS AMOUNT'];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function deleteFeature(id) {
  var query = ['MATCH (f:Feature)',
    'WHERE id(f)={id}',
    'DETACH DELETE f'
  ];
  var params = {
    id: Number(id)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getFeatureById(id) {
  console.log('w featurebyid ' + id);
  var mat = 'MATCH (f:Feature)-[]->';
  var query = [
    mat + '(p:Project)',
    mat + '(s:Status)',
    mat + '(a:Priority)',
    mat + '(u:User)',
    'WHERE id(f)={id}',
    'OPTIONAL ' + mat + '(c:Category)',
    'RETURN f as feature,p as project, s as status, a as priority, u as user,collect(c) as category'
  ];
  var params = {
    id: Number(id)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getFeatureSubjects(substring) {
  var query = ['MATCH (f:Feature)',
    'WHERE f.subject =~ ".*' + substring + '.*"',
    'RETURN f.subject AS subject, id(f) AS id'
  ];
  var params = {
    substring: String(substring)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getConnectedFeatures(projectId) {
  var query = ['MATCH (p:Project)',
    'WHERE id(p)=' + projectId,
    'MATCH (f:Feature)-[]-(p)',
    'RETURN id(f) as idf, f.subject as subject'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getFeatureListQuery(substring) {
  return ['MATCH (f:Feature)', 'WHERE f.subject contains "' + substring + '"'];
}

function getFeatureListNames(substring, page) {
  var query = getFeatureListQuery(substring);
  query.push('RETURN f.subject AS name, id(f) AS id', 'SKIP ' + page + '*4', 'LIMIT 4');
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function getFeatureListAmount(substring) {
  var query = getFeatureListQuery(substring);
  query.push('RETURN count(f) AS amount');
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}
