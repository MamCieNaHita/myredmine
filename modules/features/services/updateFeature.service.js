'use strict';
// neo4j
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var graphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new graphDatabase('http://neo4j:neopass@localhost:7474');
var bodyParser = require('body-parser');

module.exports = {
//  updateFeature: updateFeature,
//  updateProject: updateProject,
//  updateUser: updateUser,
//  updatePriority: updatePriority,
//  updateStatus: updateStatus,
//  removeCategory: removeCategory,
//  addCategories: addCategories,
//  updateUniversal: updateUniversal,
  updateUniversalNew: updateUniversalNew
};

function updateUniversalNew(id, type, newOne) {
    var query = [];
    var queryAdditional = [
        'MATCH (f:Feature) WHERE id(f)=' + id,
        'MATCH (x:' + type.name + ')',
        'OPTIONAL MATCH (f)-[r]->(x)',
        'DELETE r'
    ];
    switch (type.name) {
        case 'Category':
        query = queryAdditional;
        if (newOne.length > 0) {
            query.push(
                'WITH f',
                'MATCH (f:Feature) WHERE id(f)=' + id,
                'MATCH (c:Category) WHERE c.category IN ' + makeCat(newOne),
                'MERGE (f)-[:Category]->(c)',
                'RETURN f'
            );
        }
        break;
        case 'Priority':
        case 'Status':
        query = queryAdditional;
        query.push(
            'WITH f',
            'MATCH (x:' + type.name + ') WHERE x.' + type.label + '="' + newOne + '"',
            'MERGE (f)-[:' + type.relationship + ']->(x)',
            'RETURN f,x'
        );
        break;
        case 'User':
        case 'Project':
        query = queryAdditional;
        query.push(
            'WITH f',
            'MATCH (x:' + type.name + ') WHERE id(x)=' + newOne,
            'MERGE (f)-[:' + type.relationship + ']->(x)',
            'RETURN f,x'
        );
        break;
        case 'Feature':
        query = [
            'MATCH (f:Feature) WHERE id(f)=' + id,
            'SET f.subject = "' + newOne.subject + '"',
            'SET f.description = "' + newOne.description + '"',
            'SET f.estimatedHours =' + newOne.estimatedHours,
            'RETURN f'
        ];
        break;
        default:
        query = [];
        break;
    }
    return neodb.cypherAsync({
        query: query.join('\n'),
    });
}

function makeCat(category) {
    var result = '[';
    for (var i = 0; i < category.length; i++) {
        result += '"' + category[i] + '"';
        if (i + 1 < category.length) {
            result += ',';
        }
    }
    return result + ']';
}
