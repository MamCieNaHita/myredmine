//jshint node: true
'use strict';
var express = require('express');
var featuresService = require('./../services/features.service');
var projectService = require('./../../project/services/project.service');
var userService = require('./../../users/services/users.service');
var validationService = require('./../../validation/services/validation.service');
var Promise = require('bluebird');


var pagination = 5;

module.exports = {
  createFeature: createFeature,
  getOptions: getOptions,
  getMatchingList: getMatchingList,
  getPageList: getPageList,
  deleteFeature: deleteFeature,
  checkIfValid: checkIfValid,
  getFeatureSubjects: getFeatureSubjects,
  getConnectedFeatures: getConnectedFeatures,
  getFeatureNameList: getFeatureNameList
};

function createFeature(req, res, next) {
  checkIfValid(req.body).then(function(result) {
    if (!result) {
      featuresService.createFeature(req.body.project, req.body.subject, req.body.description, req.body.status, req.body.priority, req.body.user, req.body.category, req.body.estimatedHours).then(function(data) {
        featuresService.addCategories(data[0].idf, req.body.category).then(function(result) {
          res.send(result[0]);
        });
      });
    } else {
      console.log('sth went wrong in creation');
      res.send('WRONG DATA');
    }
  });
}

function getOptions(req, res, next) {
  featuresService.getPriority().then(function(dataPriority) {
    featuresService.getStatus().then(function(dataStatus) {
      featuresService.getCategory().then(function(dataCategory) {
        res.send({
          priority: dataPriority.map(function(element) {
            return element.p.properties.priority;
          }),
          status: dataStatus.map(function(element) {
            return element.s.properties.status;
          }),
          category: dataCategory.map(function(element) {
            return element.c.properties.category;
          })
        });
      });
    });
  });
}

function getMatchingList(req, res, next) {
  var substring;
  if (req.params[0] === undefined) {
    substring = '';
  } else {
    substring = req.params[0];
  }
  var page = req.params[1];
  if (req.params[1] === undefined) {
    substring = '';
    page = req.params[0];
  }
  featuresService.getMatchingList(substring, page, pagination).then(function(data) {
    featuresService.getMatchingListAmount(substring, page, pagination).then(function(amount) {
      for (var i = 0; i < data.length; i++) {
        data[i].amount = amount[0].pagetotal;
      }
      res.send(data);
    });
  });
}

function getPageList(req, res, next) {
  featuresService.getPageList().then(function(data) {
    res.send({
      amount: Math.ceil((data[0].AMOUNT) / pagination)
    });
  });
}

function deleteFeature(req, res, next) {
  var id = req.params[0];
  featuresService.deleteFeature(id).then(function(data) {
    res.send(data[0]);
  });
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n) && Number.isInteger(n);
}

function checkIfValid(body) {
  return new Promise(function(fulfill, reject) {
    var resultHours = isNumber(body.estimatedHours) && body.estimatedHours >= 0 && Number.isInteger(body.estimatedHours);
    var resultSubject = !!body.subject;
    var resultDescription = !!body.description;
    validationService.checkByValue(body.status, 'Status', 'status').then(function(resultStatus) {
      validationService.checkByValue(body.priority, 'Priority', 'priority').then(function(resultPriority) {
        validationService.checkById(body.project, 'Project').then(function(resultProject) {
          validationService.checkById(body.user, 'User').then(function(resultUser) {
            featuresService.getCategory().then(function(dataCategory) {
              var categoryArray = [];
              for (var k = 0; k < dataCategory.length; k++) {
                categoryArray.push(dataCategory[k].c.properties.category);
              }
              var resultCategory = true;
              for (var l = 0; l < body.category.length; l++) {
                if (categoryArray.indexOf(body.category[l]) === -1) {
                  resultCategory = false;
                }
              }
              var result = resultCategory && resultUser[0].result && resultProject[0].result && resultPriority[0].result && resultStatus[0].result && resultHours && resultDescription && resultSubject;
              fulfill(!result);
            });
          });
        });
      });
    });
  });
}

function getFeatureSubjects(req, res, next) {
  var substring = req.params[0];
  if (substring === undefined) {
    substring = '';
  }
  featuresService.getFeatureSubjects(substring).then(function(data) {
    res.send(data);
  });
}

function getConnectedFeatures(req, res, next) {
  var id = req.params[0];
  featuresService.getConnectedFeatures(id).then(function(data) {
    res.send(data);
  });
}

function getFeatureNameList(req, res, next) {
  var substring;
  if (req.params[0] === undefined) {
    substring = '';
  } else {
    substring = req.params[0];
  }
  var page = req.params[1];
  if (req.params[1] === undefined) {
    substring = '';
    page = req.params[0];
  }
  featuresService.getFeatureListNames(substring, page).then(function(data) {
    featuresService.getFeatureListAmount(substring).then(function(amount) {
      res.send(data.map(function(x) {
        x.amount = amount[0].amount;
        return x;
      }));
    });
  });
}
