'use strict';
//jshint node: true
var express = require('express');
var updateFeatureService = require('./../services/updateFeature.service');
var featuresService = require('./../services/features.service');
var controller = require('./../controllers/features.controller');
var Promise = require('bluebird');

// module.exports = updateFeature;
module.exports = newUpdateFeature;


function newUpdateFeature(req, res, next) {
  var id = req.params[0];
  var newOne = req.body;
  var oldOne;
  controller.checkIfValid(req.body).then(function(data) {
    if (data) {
      console.log('sth went wrong in update feature');
      res.send('wrong data');
      return;
    }
    featuresService.getFeatureById(id).then(function(data) {
      if (!data[0]) {
        console.log('wrong id');
        res.send('wrong id');
        return;
      }
      oldOne = parseOldOne(data[0]);
      if (newOne === oldOne) {
        res.send('nothing to change');
        return;
      }
      var truthTable = getTruthTable(oldOne, newOne);
      var types = getTypes();
      newOne.feature={
          subject: newOne.subject,
          description: newOne.description,
          estimatedHours: newOne.estimatedHours
      };
      var promiseArray=[];
      for(var key in types){
        if(types.hasOwnProperty(key)){
            if(truthTable[key]){
                promiseArray.push(updateFeatureService.updateUniversalNew(oldOne.id, types[key], newOne[key]));
            }
        }
      }
      Promise.each(promiseArray, function(result){
      }).then(function() {
          res.send('ok');
      }).catch(function(err){
        console.log('err');
        res.send('err');
      });
    });
  });
}

function parseOldOne(d) {
  var oldOne = {
    id: d.feature._id,
    subject: d.feature.properties.subject,
    description: d.feature.properties.description,
    estimatedHours: d.feature.properties.estimatedHours,
    status: d.status.properties.status,
    priority: d.priority.properties.priority,
    category: [],
    project: d.project._id,
    user: d.user._id
  };
  for (var i = 0; i < d.category.length; i++) {
    oldOne.category.push(d.category[i].properties.category);
  }
  return oldOne;
}

function getTypes() {
  return {
    status: {
      name: 'Status',
      label: 'status',
      relationship: 'Status'
    },
    priority: {
      name: 'Priority',
      label: 'priority',
      relationship: 'Priority'
    },
    category: {
      name: 'Category',
      label: 'category',
      relationship: 'Category'
    },
    user: {
      name: 'User',
      relationship: 'Author'
    },
    project: {
      name: 'Project',
      relationship: 'Project'
    },
    feature: {
      name: 'Feature'
    }
  };
}

function getTruthTable(oldOne, newOne) {
  return {
    feature: newOne.subject !== oldOne.subject || newOne.description !== oldOne.description || newOne.estimatedHours !== oldOne.estimatedHours,
    project: newOne.project !== oldOne.project,
    user: newOne.user !== oldOne.user,
    status: newOne.status !== oldOne.status,
    priority: newOne.priority !== oldOne.priority,
    category: JSON.stringify(newOne.category) !== JSON.stringify(oldOne.category),
  };
}
