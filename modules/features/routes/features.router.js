//jshint node: true
'use strict';

var express = require('express');
var controller = require('./../controllers/features.controller');
var updateFeature = require('./../controllers/updateFeature.controller');

var featureRouter = express.Router();

featureRouter.post('/feature/', controller.createFeature);
// featureRouter.post('/legacy/feature/', controller.createFeature);
featureRouter.get('/featureoptions/', controller.getOptions);
// featureRouter.get(/^\/featurelist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getMatchingList);
featureRouter.get(/^\/legacy\/featurelist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getMatchingList);
// featureRouter.get(/^\/featurelist\/\/(\w+)$/, controller.getMatchingList);
featureRouter.get(/^\/legacy\/featurelist\/\/(\w+)$/, controller.getMatchingList);
featureRouter.get('/featurepagelist/', controller.getPageList);
featureRouter.delete(/^\/feature\/([0-9]+)$/, controller.deleteFeature);
featureRouter.put(/^\/feature\/([0-9]+)$/, updateFeature);
featureRouter.get(/^\/featuresubjectlist\/([A-Za-z0-9%]+)$/, controller.getFeatureSubjects);
featureRouter.get('/featuresubjectlist/', controller.getFeatureSubjects);
featureRouter.get(/^\/featureconnected\/([0-9]+)$/, controller.getConnectedFeatures);
// featureRouter.get(/^\/featureoptions\/)
featureRouter.get(/^\/featurenewlist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getFeatureNameList);
featureRouter.get(/^\/featurenewlist\/\/([0-9]+)$/, controller.getFeatureNameList);

module.exports = featureRouter;
