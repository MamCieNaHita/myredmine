//jshint node: true
'use strict';

var express = require('express');
var controller = require('./../controllers/universal.controller');

var universalRouter = express.Router();

module.exports = universalRouter;

universalRouter.get('/testlist/', controller.getTaskMatchingList);
universalRouter.get(/^\/tasklist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getTaskMatchingList);
universalRouter.get(/^\/tasklist\/\/(\w+)$/, controller.getTaskMatchingList);
universalRouter.get(/^\/featurelist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getFeatureMatchingList);
universalRouter.post('/universallist', controller.universalMatchingList);
universalRouter.get(/^\/featurelist\/\/(\w+)$/, controller.getFeatureMatchingList);
universalRouter.get(/^\/projectlist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getProjectMatchingList);
universalRouter.get(/^\/projectlist\/\/(\w+)$/, controller.getProjectMatchingList);
// universalRouter.post('/project/', controller.createProject);
// universalRouter.post('/feature/', controller.createFeature);
universalRouter.get(/^\/featurelist\/(\w+)\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getNewMatchingListFeatures);
universalRouter.get(/^\/querylist\/(\w+)$/, controller.getQueryList);
universalRouter.get(/^\/querylist2\/(\w+)$/,controller.getAllRelated);
universalRouter.get(/^\/getallamounts$/, controller.getAllAmounts);
