'use strict';
//neo4j
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var GraphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new GraphDatabase('http://neo4j:neopass@localhost:7474');
module.exports = {
  getAllRelated: getAllRelated
};


/*function getAllRelated(relationships) {
  var query = relationships.map(function(element) {
    return 'MATCH (' + element.name + ':' + element.type + ')';
  });
  query.push('RETURN ' +
    relationships.map(function(element) {
      return 'collect(' + element.name + '.' + element.label + ') as ' + element.name;
    }).join(',')
  );
  return neodb.cypherAsync({
    query: query.join('\n'),
  });
}*/

function getRelated(relationship) {
  return new Promise(function(fulfill, reject) {
    var query = ['MATCH (x:' + relationship.type + ')',
      'RETURN collect(x.' + relationship.label + ') as ' + relationship.label
    ];
    neodb.cypherAsync({
      query: query.join('\n')
  }).then(function(response){
      fulfill({
          type: relationship.type,
          label: relationship.name[0].toUpperCase() + relationship.name.substr(1),
          name: relationship.name,
          tab: response[0][relationship.label]}
      );
  });
});
}

function getAllRelated(relationships) {
  return new Promise(function(fulfill, reject) {
    var promiseArray = [];
    relationships.forEach(function(relationship) {
      if (!relationship.getId) {
        promiseArray.push(getRelated(relationship));
      }
    });
    var result = [];
    Promise.each(promiseArray, function(element) {
      result.push(element);
    }).then(function(x) {
      fulfill(result);
    });
  });
}
