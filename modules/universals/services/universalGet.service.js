'use strict';
//neo4j
//jshint node: true

var neo4j = require('neo4j');
var Promise = require('bluebird');
var graphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new graphDatabase('http://neo4j:neopass@localhost:7474');

module.exports = {
  getMatchingList: getMatchingList,
  getMatchingListAmount: getMatchingListAmount,
  getNewMatchingList: getNewMatchingList,
  universalMatchingList: universalMatchingList,
  getAllAmounts: getAllAmounts
};

function getUntilReturn(body, substring) {
  var query = getStartMatchingQuery(body);
  query.push(getWithMatchingQuery(body));
  query = query.concat(getLimitedMatchingQuery(body, substring));
  return query;
}

function getMatchingList(body, substring, page, pagination) {
  var query = getUntilReturn(body, substring);
  query = query.concat(getReturnMatchingQuery(body, page, pagination));
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function getMatchingListAmount(body, substring) {
  var query = getUntilReturn(body, substring);
  var secondWith = 'WITH ' + body.main.label;
  body.relationships.forEach(function(option) {
    if (option.label === 'category') {
      secondWith += ', collect(category.category) as category';
    }
  });
  query.push(secondWith);
  query.push(getReturnMatchingAmount(body));
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function getStartMatchingQuery(body) {
  var result = [];
  if (body.relationships.length === 0) {
    result = ['MATCH (' + body.main.label + ':' + body.main.type + ')'];
  } else {
    body.relationships.forEach(function(option) {
      var toAdd = 'MATCH (' + body.main.label + ':' + body.main.type + ')-[]->(' + option.name + ':' + option.type + ')';
      if (option.label === 'category') {
        result.push('OPTIONAL ' + toAdd);
      } else {
        result.push(toAdd);
      }
    });
  }
  return result;
}

function getWithMatchingQuery(body) {
  var result = 'WITH ' + body.main.label;
  body.relationships.forEach(function(option) {
    result += ', ' + option.name;
  });
  return result;
}

function getLimitedMatchingQuery(body, substring) {
  var limitation = 'WHERE ';
  var fromMain = '';
  var comma = false;
  body.main.labels.forEach(function(label) {
    if (comma) {
      fromMain += ' OR ';
    }
    fromMain += body.main.label + '.' + label + ' CONTAINS "' + substring + '"';
    comma = true;
  });
  var fromRelationships = '';
  comma = false;
  body.relationships.forEach(function(option) {
    if (option.label !== 'category') {
      if (comma) {
        fromRelationships += ' OR ';
      }
      fromRelationships += option.name + '.' + option.label + ' CONTAINS "' + substring + '"';
      comma = true;
    }
  });
  var fromBody;
  if (!!fromRelationships) {
    fromBody = [fromMain, fromRelationships];
    fromBody = fromBody.join(' OR ');
  } else {
    fromBody = fromMain;
  }
  return limitation + fromBody;
}

function getReturnMatchingQuery(body, page, pagination) {
  var forReturn = 'RETURN ' + body.main.label;
  body.relationships.forEach(function(option) {
    if (option.label !== 'category') {
      forReturn += ',' + option.name + '.' + option.label + ' as ' + option.name;
      if (option.getId) {
        forReturn += ', id(' + option.name + ') as ' + option.name + 'id';
      }
    } else {
      forReturn += ', collect(' + option.name + '.category) as category';
    }
  });
  var forPagination = 'ORDER BY id(' + body.main.label + ') SKIP ' + page * pagination + ' LIMIT ' + pagination;
  return [forReturn, forPagination];
}

function getReturnMatchingAmount(body) {
  return 'RETURN count(' + body.main.label + ') as pagetotal';
}
//===========================================================================================================================================
function getMatchingIdList(body, where, substring, page, pagination) {
  var query = getQueryToId(body, where, substring);
  query.push('RETURN id(' + body.main.label + ') as id SKIP ' + page * pagination + ' LIMIT ' + pagination);
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function getMatchingIdListAmount(body, where, substring) {
  var query = getQueryToId(body, where, substring);
  query.push('RETURN count(' + body.main.label + ') as amount');
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function getFromMatchingId(body, id) {
  var query = [];
  var idString = '[' + id.join(',') + ']';
  query = ['MATCH (' + body.main.label + ':' + body.main.type + ') WHERE id(' + body.main.label + ') in ' + idString];
  body.relationships.forEach(function(option) {
    var toAdd = 'MATCH (' + body.main.label + ':' + body.main.type + ')-[]->(' + option.name + ':' + option.type + ')';
    if (option.label === 'category') {
      query.push('OPTIONAL ' + toAdd);
    } else {
      query.push(toAdd);
    }
  });
  var forReturn = 'RETURN ' + body.main.label;
  body.relationships.forEach(function(option) {
    if (option.label !== 'category') {
      forReturn += ',' + option.name + '.' + option.label + ' as ' + option.name;
      if (option.getId) {
        forReturn += ', id(' + option.name + ') as ' + option.name + 'id';
      }
    } else {
      forReturn += ', collect(' + option.name + '.category) as category';
    }
  });
  forReturn += ' ORDER BY id(' + body.main.label + ')';
  query.push(forReturn);
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function getQueryToId(body, where, substring) {
  var query = [];
  var index = body.main.labels.indexOf(where);
  if (index !== -1) {
    query = ['MATCH (' + body.main.label + ':' + body.main.type + ') WHERE ' + body.main.label + '.' + body.main.labels[index] + ' CONTAINS "' + substring + '"'];
  } else {
    var relationship;
    body.relationships.forEach(function(option) {
      if (option.name === where) {
        relationship = option;
      }
    });
    query = [
      'MATCH (' + body.main.label + ':' + body.main.type + ')-[]->(' + relationship.name + ':' + relationship.type + ')' + ' WHERE ' + relationship.name + '.' + relationship.label + ' CONTAINS "' + substring + '"'
    ];
    if (relationship.label === 'category') {
      query.push('WITH ' + body.main.label + ',collect(' + relationship.name + ') as c');
    }
  }
  return query;
}


function getNewMatchingList(body, where, substring, order, page, pagination) {
  return new Promise(function(fulfill, reject) {
    getMatchingIdList(body, where, substring, page, pagination).then(function(id) {
      getMatchingIdListAmount(body, where, substring).then(function(amount) {
        getFromMatchingId(body, id).then(function(response) {
          response.forEach(function(element) {
            element.amount = amount[0].amount;
          });
          fulfill(response);
        });
      });
    });
  });
}

function newestMatchingIdList(body, tab, substring, order, page, pagination) {
  var query = newestQueryToId(body, tab, substring, false);
  query.push('WITH count('+body.main.label+') as amount');
  var toQuery= newestQueryToId(body,tab,substring, true);
  toQuery.forEach(function(element){
      query.push(element);
  });
  // var toQuery = 'RETURN id(' + body.main.label + ') as id ORDER BY ' + order.by;
  // if (order.desc) {
  //   toQuery += ' DESC';
  // }
  // toQuery += ' SKIP ' + page * pagination + ' LIMIT ' + pagination;
  var forReturn = 'RETURN ' + body.main.label;
  body.relationships.forEach(function(option) {
    if (option.label !== 'category') {
      forReturn += ',' + option.name + '.' + option.label + ' as ' + option.name;
      if (option.getId) {
        forReturn += ', id(' + option.name + ') as ' + option.name + 'id';
      }
    } else {
      forReturn += ', category';
    }
  });
  forReturn += ', amount'
  forReturn += ' ORDER BY ' + order.by;
  if (order.desc) {
    forReturn += ' DESC';
  }
  forReturn += ' SKIP ' + page * pagination + ' LIMIT ' + pagination;
  query.push(forReturn);
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

// function newestMatchingIdListAmount(body, tab, substring) {
//   var query = newestQueryToId(body, tab, substring);
//   query.push('RETURN count(' + body.main.label + ') as amount');
//   return neodb.cypherAsync({
//     query: query.join('\n')
//   });
// }

function newestQueryToId(body, tab, substring, isAmount) {
  var query = [];
  var toQuery = 'MATCH (' + body.main.label + ':' + body.main.type + ') WHERE ';
  var toToQuery = [];
  body.main.labels.forEach(function(label) {
    if (label === label.toLowerCase()) {
      toToQuery.push(body.main.label + '.' + label + ' CONTAINS "' + substring + '"');
    }
  });
  toQuery += toToQuery.join(' OR ');
  query.push(toQuery);
  var matchAll = body.relationships.map(function(relationship) {
    if (relationship.name !== 'category') {
      return 'MATCH (' + body.main.label + ')-[]->(' + relationship.name + ':' + relationship.type + ')';
    } else {
      return 'OPTIONAL MATCH (' + body.main.label + ')-[]->(' + relationship.name + ':' + relationship.type + ')';

    }
  });
  matchAll.forEach(function(element) {
    query.push(element);
  });
  var amount='';
  if(isAmount)
  {
      amount=', amount';
  }
  query.push('WITH ' + body.main.label + ',' + body.relationships.map(function(relationship) {
    return relationship.name;
}).join(',') + amount);
  var nextOne = tab.map(function(option) {
    var toAdd;
    if (option.label !== 'category') {
      toAdd = 'MATCH (' + body.main.label + ')-[]->(' + option.name + ') WHERE ' + option.name + '.' + option.label + ' IN [' + option.tab.map(function(element) {
        return '"' + element + '"';
      }).join(',') + ']';
      return toAdd;
    } else {
      toAdd = 'MATCH (' + body.main.label + '),(' + option.name + ') WHERE NOT (' + body.main.label + ')-[:' + option.type + ']->() or (' + body.main.label + ')-[]->(' + option.name + ') and ' + option.name + '.' + option.label + ' in [' +
        option.tab.map(function(element) {
          return '"' + element + '"';
        }).join(',') + ']';
      return toAdd;
    }
  });
  nextOne.forEach(function(element) {
    query.push(element);
  });
  var withQuery = 'WITH ' + body.main.label;
  body.relationships.forEach(function(option) {
    if (option.label === 'category') {
      withQuery += ', ' + 'collect(' + option.name + '.category) as category';
    } else {
      withQuery += ', ' + option.name;
    }
  });
  if(isAmount){
      withQuery+=', amount'
  }
  query.push(withQuery);
  return query;
}

//
// function universalMatchingList(body, tab, substring, order, page, pagination) {
//   return new Promise(function(fulfill, reject) {
//     newestMatchingIdList(body, tab, substring, order, page, pagination).then(function(id) {
//       var fixedId = id.map(function(id) {
//         return id.id;
//       });
//       newestMatchingIdListAmount(body, tab, substring).then(function(amount) {
//         getFromMatchingId(body, fixedId).then(function(response) {
//           response.forEach(function(element) {
//             element.amount = amount[0].amount;
//           });
//           fulfill(response);
//         });
//       });
//     });
//   });
// }

function universalMatchingList(body, tab, substring, order, page, pagination) {
  return new Promise(function(fulfill, reject) {
    newestMatchingIdList(body, tab, substring, order, page, pagination).then(function(response) {
    //  newestMatchingIdListAmount(body, tab, substring).then(function(amount) {
    //    response.forEach(function(element) {
        //  element.amount = amount[0].amount;
    //    });
        fulfill(response);
     // });
    });
  });
}

function getAllAmounts(types){
  var withAlready=[];
  var query=[]
    types.forEach(function(type){
      query.push('MATCH ('+type+':'+type+')');
      query.push('WITH '+withAlready.join('') + ' count('+type+') as ' +type);
      withAlready.push(type+',');
    });
    query.push('RETURN '+ types.join(','));
    return neodb.cypherAsync({
      query: query.join('\n')
    }).then(function(response){
      var newResult={};
      for(var type in response[0]){
        if(response[0].hasOwnProperty(type)){
          var temp=response[0][type];
          newResult[type.toLowerCase()]=temp;
        }
      }
      return newResult;
    });
}
