'use strict';
//neo4j
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var GraphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new GraphDatabase('http://neo4j:neopass@localhost:7474');

module.exports = {
  createOne: createOne
};

function createOne(body, data) {
  return new Promise(function(fulfill, reject) {
    createMain(body.main, data).then(function(id) {
      if (!!body.relationships.length) {
        addOptions(body, data, id[0].id).then(function(response) {
          addCategories(body, data.category, id[0].id).then(function(response) {
            fulfill('ok');
          });
        });
      } else {
        fulfill('ok');
      }
    });
  });
}

function createMain(main, data) {
  var creation = 'CREATE (' + main.label + ':' + main.type + '{';
  main.labels.forEach(function(element, index) {
    if (index) {
      creation += ',';
    }
    creation += element + ':"' + data[element] + '"';
  });
  creation += '})';
  var query = [creation, 'RETURN id(' + main.label + ') as id'];
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function addOptions(body, data, id) {
  var match = 'MATCH (' + body.main.label + ') WHERE id(' + body.main.label + ')=' + id;
  var matchMerge = '';
  body.relationships.forEach(function(element, index) {
    if (element.label !== 'category') {
      if (index) {
        matchMerge += '\n ';
      }
      matchMerge += 'WITH ' + body.main.label + ' ';
      matchMerge += 'MATCH (' + element.name + ':' + element.type + ') WHERE ';
      if (element.getId) {
        matchMerge += 'id(' + element.name + ')=' + data[element.name];
      } else {
        matchMerge += element.name + '.' + element.label + '="' + data[element.label] + '"';
      }
      matchMerge += ' MERGE (' + body.main.label + ')-[:' + element.relationship + ']->(' + element.name + ')';
    }
  });
  var query = [match, matchMerge];
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function addCategories(body, dataCategories, id) {
  var main = body.main;
  function getCat(bodyCat) {
    var result;
    bodyCat.forEach(function(element) {
      if (element.label === 'category') {
        result = element;
      }
    });
    return result;
  }
  var cat = getCat(body.relationships);
  var query = ['MATCH (' + main.label + ':' + main.type + ') WHERE id(' + main.label + ')=' + id];
  var catString = '[' + dataCategories.map(function(cats) {
    return '"' + cats + '"';
  }).join(',') + ']';
  query.push('MATCH (' + cat.name + ':' + cat.type + ') WHERE ' + cat.name + '.' + cat.label + ' IN ' + catString);
  query.push('MERGE (' + body.name + ')-[:' + cat.relationship + ']->(' + cat.name + ')');
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}
