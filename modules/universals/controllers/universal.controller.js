'use strict';
//jshint node: true
var universalGetService = require('./../services/universalGet.service');
var universalCreateService = require('./../services/universalCreate.service');
var universalAdditionalsService = require('./../services/universalAdditionals.service');
var bodies = require('./body.js');
var Promise = require('bluebird');

module.exports = {
  getTaskMatchingList: getTaskMatchingList,
  getFeatureMatchingList: getFeatureMatchingList,
  getProjectMatchingList: getProjectMatchingList,
  createProject: createProject,
  createFeature: createFeature,
  getNewMatchingListFeatures: getNewMatchingListFeatures,
  getQueryList: getQueryList,
  getAllRelated: getAllRelated,
  universalMatchingList: universalMatchingList,
  getAllAmounts: getAllAmounts
};
var pagination = 5;

function getTaskMatchingList(req, res, next) {
  var page = getPage(req);
  var substring = getSubstring(req);
  getMatchingList('task', substring, page).then(function(data) {
    res.send(data);
  });
}

function getFeatureMatchingList(req, res, next) {
  var page = getPage(req);
  var substring = getSubstring(req);
  getMatchingList('feature', substring, page).then(function(data) {
    res.send(data);
  });
}

function getProjectMatchingList(req, res, next) {
  var page = getPage(req);
  var substring = getSubstring(req);
  getMatchingList('project', substring, page).then(function(data) {
    res.send(data);
}).catch(function(err){
    console.log(err);
});
}

function getMatchingList(type, substring, page) {
  var body = bodies[type];
  return new Promise(function(fulfill, reject) {
    universalGetService.getMatchingList(body, substring, page, pagination).then(function(data) {
      universalGetService.getMatchingListAmount(body, substring).then(function(amount) {
        fulfill(
          data.map(function(element) {
            return {
              id: element.project._id,
              name: element.project.properties.name,
              identifier: element.project.properties.identifier,
              description: element.project.properties.description,
              amount: amount[0].pagetotal
            };
          })
        );
      });
    });
  });
}

function getSubstring(req) {
  var substring;
  if (req.params[0] === undefined) {
    substring = '';
  } else {
    substring = req.params[0];
  }
  if (req.params[1] === undefined) {
    substring = '';
  }
  return substring;
}

function getPage(req) {
  var page = req.params[1];
  if (req.params[1] === undefined) {
    page = req.params[0];
  }
  return page;
}

function createProject(req, res, next) {
  universalCreateService.createOne(bodies['project'], req.body).then(function(response) {
    res.send(response);
  });
}

function createFeature(req, res, next) {
  universalCreateService.createOne(bodies['feature'], req.body).then(function(response) {
    res.send(response);
  });
}

function getNewMatchingListFeatures(req, res, next) {
  var where = req.params[0];
  var substring = req.params[1];
  var page = req.params[2];
  universalGetService.getNewMatchingList(bodies['feature'], where, substring, page, pagination).then(function(response) {
    res.send(response);
  });
}

function getQueryList(req, res, next) {
  var result = [];
  if (!bodies.hasOwnProperty(req.params[0])) {
    res.send([]);
    return;
  }
  bodies[req.params[0]].main.labels.forEach(function(label) {
    if (label === label.toLowerCase()) {
      result.push(label);
    }
  });
  result = Array.prototype.concat(result, bodies[req.params[0]].relationships.map(function(element) {
    return element.name;
  }));
  res.send(result);
}

function getAllRelated(req, res, next) {
  if (!bodies.hasOwnProperty(req.params[0])) {
    res.send([]);
    return;
  }
  universalAdditionalsService.getAllRelated(bodies[req.params[0]].relationships).then(function(result) {
    res.send(result);
  });
}

function universalMatchingList(req, res, next) {
  var order = {
    desc: req.body.desc
  };
  var orderby = req.body.order.split(' ').map(function(substring, index) {
    if (index === 0) {
      return substring.toLowerCase();
    } else {
      return substring;
    }
  }).join('');
  bodies[req.body.type].main.labels.forEach(function(label) {
      if(label===orderby)
      {
          order.by=bodies[req.body.type].main.label+'.'+label;
      }
  });
  if(order.by===undefined)
  {
      bodies[req.body.type].relationships.forEach(function(relationship) {
          console.log(relationship.name + '===' + orderby);
          if(relationship.name===orderby){
              order.by=relationship.name+'.'+relationship.label;
          }
      });
  }
  universalGetService.universalMatchingList(bodies[req.body.type], req.body.tab, req.body.substring, order, req.body.page, pagination).then(function(result) {
    res.send(
      result.map(function(element) {
        var toReturn = {};
        toReturn.amount = element.amount;
        toReturn.id = element[req.body.type]._id;
        bodies[req.body.type].main.labels.forEach(function(label) {
          toReturn[label] = element[req.body.type].properties[label];
        });
        bodies[req.body.type].relationships.forEach(function(rel) {
          if (!!rel.getId) {
            toReturn[rel.name + 'id'] = element[rel.name + 'id'];
            toReturn[rel.name + 'name'] = element[rel.name];
          } else {
            toReturn[rel.label] = element[rel.label];
          }
        });
        return toReturn;
      })
    );
  });
}

function getAllAmounts(req,res,next){
  universalGetService.getAllAmounts(['Project','Feature','Task']).then(function(response){
    res.json(response);
  }).catch(function(err){
    res.json({
      success: false,
      error: err
    });
  });
}
