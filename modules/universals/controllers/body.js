module.exports = {
  project: {
    main: {
      type: 'Project',
      label: 'project',
      labels: [
        'name',
        'identifier',
        'description'
      ]
    },
    relationships: []
  },
  feature: {
    main: {
      type: 'Feature',
      label: 'feature',
      labels: [
        'subject',
        'description',
        'estimatedHours'
      ]
    },
    relationships: [{
      name: 'status',
      type: 'Status',
      label: 'status',
      relationship: 'Status',
      getId: false
    }, {
      name: 'priority',
      type: 'Priority',
      label: 'priority',
      relationship: 'Priority',
      getId: false
    }, {
      name: 'user',
      type: 'User',
      label: 'name',
      relationship: 'Author',
      getId: true
    }, {
      name: 'project',
      type: 'Project',
      label: 'name',
      relationship: 'Project',
      getId: true
    }, {
      name: 'category',
      type: 'Category',
      label: 'category',
      relationship: 'Category',
      getId: false
    }]
  },
  task: {
    main: {
      type: 'Task',
      label: 'task',
      labels: [
        'subject',
        'description',
        'estimatedHours',
        'doneRatio',
        'startDate',
        'dueDate'
      ]
    },
    relationships: [{
      name: 'feature',
      type: 'Feature',
      label: 'subject',
      relationship: 'Feature',
      getId: true
    }, {
      name: 'status',
      type: 'taskStatus',
      label: 'status',
      relationship: 'taskStatus',
      getId: false
    }, {
      name: 'tracker',
      type: 'taskTracker',
      label: 'tracker',
      relationship: 'taskTracker',
      getId: false
    }, {
      name: 'priority',
      type: 'taskPriority',
      label: 'priority',
      relationship: 'taskPriority',
      getId: false
    }, {
      name: 'user',
      type: 'User',
      label: 'name',
      relationship: 'taskAuthor',
      getId: true
    }, {
      name: 'category',
      type: 'taskCategory',
      label: 'category',
      relationship: 'taskCategory',
      getId: false
    }]
  }
};
