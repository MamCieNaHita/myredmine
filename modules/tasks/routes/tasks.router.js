//jshint node: true
'use strict';

var express = require('express');
var controller = require('./../controllers/tasks.controller');
var updateTask = require('./../controllers/updateTask.controller');

var tasksRouter = express.Router();

module.exports = tasksRouter;

tasksRouter.post('/task/', controller.createTask);
//tasksRouter.get(/^\/tasklist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getMatchingList);
tasksRouter.get(/^\/legacy\/tasklist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getMatchingList);
//tasksRouter.get(/^\/tasklist\/\/(\w+)$/, controller.getMatchingList);
tasksRouter.get(/^\/legacy\/tasklist\/\/(\w+)$/, controller.getMatchingList);
tasksRouter.get('/taskoptions/', controller.getOptions);
tasksRouter.get('/taskpagelist/', controller.getPageList);
tasksRouter.delete(/^\/task\/([0-9]+)$/, controller.deleteTask);
tasksRouter.get(/^\/taskproject\/([0-9]+)$/, controller.getTaskFromProject);
tasksRouter.get(/^\/taskfeature\/([0-9]+)$/, controller.getTaskFromFeature);
tasksRouter.put(/^\/task\/([0-9]+)$/, updateTask);
