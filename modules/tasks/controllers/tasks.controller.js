'use strict';
//jshint node: true
var express = require('express');
var tasksService = require('./../services/tasks.service');
var validationService = require('./../../validation/services/validation.service');
var Promise = require('bluebird');

module.exports = {
  createTask: createTask,
  getMatchingList: getMatchingList,
  getOptions: getOptions,
  getPageList: getPageList,
  deleteTask: deleteTask,
  getTaskFromProject: getTaskFromProject,
  getTaskFromFeature: getTaskFromFeature,
  checkIfValid: checkIfValid
};

var pagination = 5;

function createTask(req, res, next) {
  checkIfValid(req.body).then(function(result) {
    if (!result) {
      tasksService.createTask(req.body.feature,
        req.body.subject,
        req.body.description,
        req.body.tracker,
        req.body.status,
        req.body.priority,
        req.body.user,
        req.body.category,
        req.body.startDate,
        req.body.dueDate,
        req.body.doneRatio,
        req.body.estimatedHours).then(function(data) {
        tasksService.addCategories(data[0].idt, req.body.category).then(function(result) {
          res.send(result[0]);
        });
      });
    } else {
      console.log('sth went wrong in creation');
      res.send('WRONG DATA');
    }
  });
}

function getMatchingList(req, res, next) {
  var substring;
  if (req.params[0] === undefined) {
    substring = '';
  } else {
    substring = req.params[0];
  }
  var page = req.params[1];
  if (req.params[1] === undefined) {
    substring = '';
    page = req.params[0];
  }
  tasksService.getMatchingList(substring, page, pagination).then(function(data) {
      tasksService.getMatchingListAmount(substring, page, pagination).then(function(amount) {
          for(var i=0; i<data.length; i++)
          {
              data[i].amount=amount[0].pagetotal;
          }
          res.send(data);
      });
  });
}

function getOptions(req, res, next) {
  tasksService.getCategory().then(function(dataCategory) {
    tasksService.getStatus().then(function(dataStatus) {
      tasksService.getPriority().then(function(dataPriority) {
        tasksService.getTracker().then(function(dataTracker) {
          res.send({
            category: dataCategory[0].category,
            status: dataStatus[0].status,
            priority: dataPriority[0].priority,
            tracker: dataTracker[0].tracker
          });
        });
      });
    });
  });
}

function getPageList(req, res, next) {
  tasksService.getPageList().then(function(data) {
    res.send({
      amount: Math.ceil((data[0].AMOUNT) / pagination)
    });
  });
}

function deleteTask(req, res, next) {
  var id = req.params[0];
  tasksService.deleteTask(id).then(function(data) {
    res.send(data[0]);
  });
}

function getTaskFromProject(req, res, next) {
  var id = req.params[0];
  tasksService.getTaskFromProject(id).then(function(data) {
    res.send(data);
  });
}

function getTaskFromFeature(req, res, next) {
  var id = req.params[0];
  tasksService.getTaskFromFeature(id).then(function(data) {
    res.send(data);
  });
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n) && Number.isInteger(n);
}

function checkIfValid(body) {
  return new Promise(function(fulfill, reject) {
    var resultHours = isNumber(body.estimatedHours) && body.estimatedHours >= 0;
    var resultStart = isNumber(body.startDate) && body.startDate >= 0;
    var resultDue = isNumber(body.dueDate) && body.dueDate >= 0;
    var resultRatio = isNumber(body.doneRatio) && body.doneRatio >= 0 && body.doneRatio <= 100;
    var resultStartDue = body.dueDate >= body.startDate;
    var resultSubject = !!body.subject;
    var resultDescription = !!body.description;
    var resultTask = resultHours && resultStart && resultDue && resultStartDue && resultSubject && resultDescription && resultRatio;
    validationService.checkByValue(body.status, 'taskStatus', 'status').then(function(resultStatus) {
      validationService.checkByValue(body.priority, 'taskPriority', 'priority').then(function(resultPriority) {
        validationService.checkByValue(body.tracker, 'taskTracker', 'tracker').then(function(resultTracker) {
          validationService.checkById(body.feature, 'Feature').then(function(resultFeature) {
            validationService.checkById(body.user, 'User').then(function(resultUser) {
              tasksService.getCategory().then(function(dataCategory) {
                var categoryArray = dataCategory[0].category;
                var resultCategory = true;
                for (var l = 0; l < body.category.length; l++) {
                  if (categoryArray.indexOf(body.category[l]) === -1) {
                    resultCategory = false;
                  }
                }
                var result = resultCategory && resultUser[0].result && resultFeature[0].result && resultTracker[0].result && resultPriority[0].result && resultStatus[0].result && resultTask;
                fulfill(!result);
              });
            });
          });
        });
      });
    });
  });
}
