'use strict';
//jshint node: true
var express = require('express');
var updateTaskService = require('./../services/updateTask.service');
var tasksService = require('./../services/tasks.service');
var controller = require('./../controllers/tasks.controller');
var Promise = require('bluebird');

module.exports = newUpdateTask;

function newUpdateTask(req, res, next) {
  var id = req.params[0];
  var newOne = req.body;
  var oldOne;
  controller.checkIfValid(req.body).then(function(data) {
    if (data) {
      console.log('sth went wrong in update feature');
      res.send('wrong data');
      return;
    }
    tasksService.getTaskById(id).then(function(data) {
      if (!data[0]) {
        console.log('wrong id');
        res.send('wrong id');
        return;
      }
      oldOne = parseOldOne(data[0]);
      if (newOne === oldOne) {
        res.send('nothing to change');
        return;
      }
      var truthTable = getTruthTable(oldOne, newOne);
      var types = getTypes();
      newOne.task = {
        subject: newOne.subject,
        description: newOne.description,
        estimatedHours: newOne.estimatedHours,
        doneRatio: newOne.doneRatio,
        startDate: newOne.startDate,
        dueDate: newOne.dueDate
      };
      var promiseArray = [];
      for (var key in types) {
        if (types.hasOwnProperty(key)) {
          if (truthTable[key]) {
            promiseArray.push(updateTaskService.updateUniversal(oldOne.id, types[key], newOne[key]));
          }
        }
      }
      Promise.each(promiseArray, function(result) {
      }).then(function() {
        res.send('ok');
      });
    });
  });
}

function parseOldOne(d) {
  var oldOne = {
    id: d.task._id,
    subject: d.task.properties.subject,
    description: d.task.properties.description,
    estimatedHours: d.task.properties.estimatedHours,
    doneRatio: d.task.properties.doneRatio,
    startDate: d.task.properties.startDate,
    dueDate: d.task.properties.dueDate,
    status: d.status,
    priority: d.priority,
    feature: d.feature,
    user: d.user,
    tracker: d.tracker,
    category: d.category.slice(0)
  };
  return oldOne;
}

function getTypes() {
  return {
    status: {
      name: 'taskStatus',
      label: 'status',
      relationship: 'taskStatus'
    },
    priority: {
      name: 'taskPriority',
      label: 'priority',
      relationship: 'taskPriority'
    },
    category: {
      name: 'taskCategory',
      label: 'category',
      relationship: 'taskCategory'
    },
    tracker: {
      name: 'taskTracker',
      label: 'tracker',
      relationship: 'taskTracker'
    },
    user: {
      name: 'User',
      relationship: 'Author'
    },
    feature: {
      name: 'Feature',
      relationship: 'Feature'
    },
    task: {
      name: 'Task'
    }
  };
}

function getTruthTable(oldOne, newOne) {
  return {
    task: newOne.subject !== oldOne.subject || newOne.description !== oldOne.description || newOne.estimatedHours !== oldOne.estimatedHours || newOne.doneRatio !== oldOne.doneRatio || newOne.startDate !== oldOne.startDate || newOne.dueDate !== oldOne.dueDate,
    feature: newOne.feature !== oldOne.feature,
    user: newOne.user !== oldOne.user,
    status: newOne.status !== oldOne.status,
    priority: newOne.priority !== oldOne.priority,
    tracker: newOne.tracker !== oldOne.tracker,
    category: JSON.stringify(newOne.category) !== JSON.stringify(oldOne.category),
  };
}


// function updateTask(req, res, next) {
//   var id = req.params[0];
//   var newOne = req.body;
//   var oldOne;
//   tasksService.getTaskById(id).then(function(data) {
//     var d = data[0];
//     if (!d) {
//       console.log('wrong id');
//       res.send('wrong id');
//       return;
//     }
//     controller.checkIfValid(req.body).then(function(validResult) {
//       if (validResult) {
//         console.log('sth wrong with update task');
//         res.send('sth wrong with update task');
//         return;
//       } else {
//         oldOne = {
//           id: d.task._id,
//           subject: d.task.properties.subject,
//           description: d.task.properties.description,
//           estimatedHours: d.task.properties.estimatedHours,
//           doneRatio: d.task.properties.doneRatio,
//           startDate: d.task.properties.startDate,
//           dueDate: d.task.properties.dueDate,
//           status: d.status,
//           priority: d.priority,
//           feature: d.feature,
//           user: d.user,
//           tracker: d.tracker,
//           category: d.category.slice(0)
//         };
//         if (newOne !== oldOne) {
//           if (newOne.subject !== oldOne.subject || newOne.description !== oldOne.description || newOne.estimatedHours !== oldOne.estimatedHours || newOne.startDate !== oldOne.startDate || newOne.dueDate !== oldOne.dueDate || newOne.doneRatio !== oldOne.doneRatio) {
//             updateTaskService.updateTask(oldOne.id, {
//               subject: newOne.subject,
//               description: newOne.description,
//               estimatedHours: newOne.estimatedHours,
//               startDate: newOne.startDate,
//               dueDate: newOne.dueDate,
//               doneRatio: newOne.doneRatio
//             });
//           }
//           if (newOne.status !== oldOne.status) {
//             updateTaskService.updateStatus(oldOne.id, newOne.status);
//           }
//           if (newOne.priority !== oldOne.priority) {
//             updateTaskService.updatePriority(oldOne.id, newOne.priority);
//           }
//           if (newOne.tracker !== oldOne.tracker) {
//             updateTaskService.updateTracker(oldOne.id, newOne.tracker);
//           }
//           if (newOne.user !== oldOne.user) {
//             updateTaskService.updateUser(oldOne.id, newOne.user);
//           }
//           if (newOne.feature !== oldOne.feature) {
//             updateTaskService.updateFeature(oldOne.id, newOne.feature);
//           }
//           if (JSON.stringify(newOne.category) !== JSON.stringify(oldOne.category)) {
//             if (oldOne.category.length) {
//               updateTaskService.removeCategory(oldOne.id).then(function(data) {
//                 updateTaskService.addCategories(oldOne.id, newOne.category);
//               });
//             } else {
//               updateTaskService.addCategories(oldOne.id, newOne.category);
//             }
//           }
//         }
//       }
//     });
//   });
// }
