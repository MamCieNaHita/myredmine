'use strict';
// neo4j
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var graphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new graphDatabase('http://neo4j:neopass@localhost:7474');
var bodyParser = require('body-parser');

module.exports = {
  // updateTask: updateTask,
  // updateFeature: updateFeature,
  // updateUser: updateUser,
  // updatePriority: updatePriority,
  // updateTracker: updateTracker,
  // updateStatus: updateStatus,
  // removeCategory: removeCategory,
  // addCategories: addCategories
  updateUniversal: updateUniversal
};

function updateUniversal(id, type, newOne) {
  var query = [];
  var queryAdditional = [
    'MATCH (t:Task) WHERE id(t)=' + id,
    'MATCH (x:' + type.name + ')',
    'OPTIONAL MATCH (t)-[r]->(x)',
    'DELETE r'
  ];
  switch (type.name) {
    case 'taskCategory':
      query = queryAdditional;
      if (newOne.length > 0) {
        query.push(
          'WITH f',
          'MATCH (t:Task) WHERE id(t)=' + id,
          'MATCH (c:taskCategory) WHERE c.category IN ' + makeCat(newOne),
          'MERGE (t)-[:taskCategory]->(c)',
          'RETURN t'
        );
      }
      break;
    case 'taskPriority':
    case 'taskStatus':
    case 'taskTracker':
      query = queryAdditional;
      query.push(
        'WITH t',
        'MATCH (x:' + type.name + ') WHERE x.' + type.label + '="' + newOne + '"',
        'MERGE (t)-[:' + type.relationship + ']->(x)',
        'RETURN t,x'
      );
      break;
    case 'User':
    case 'Feature':
      query = queryAdditional;
      query.push(
        'WITH t',
        'MATCH (x:' + type.name + ') WHERE id(x)=' + newOne,
        'MERGE (t)-[:' + type.relationship + ']->(x)',
        'RETURN t,x'
      );
      break;
    case 'Task':
      query = [
        'MATCH (t:Task) WHERE id(t)=' + id,
        'SET t.subject = "' + newOne.subject + '"',
        'SET t.description = "' + newOne.description + '"',
        'SET t.estimatedHours =' + newOne.estimatedHours,
        'SET t.doneRatio = ' + newOne.doneRatio,
        'SET t.startDate = ' + newOne.startDate,
        'SET t.dueDate =' + newOne.dueDate,
        'RETURN t'
      ];
      break;
    default:
      query = [];
      break;
  }
  return neodb.cypherAsync({
    query: query.join('\n'),
  });
}

function makeCat(category) {
  var result = '[';
  for (var i = 0; i < category.length; i++) {
    result += '"' + category[i] + '"';
    if (i + 1 < category.length) {
      result += ',';
    }
  }
  return result + ']';
}




// function updateTask(id, task) {
//   var query = [
//     'MATCH (t:Task)',
//     'WHERE id(t)={id}',
//     'SET t.subject = {subject}',
//     'SET t.description = {description}',
//     'SET t.estimatedHours = {estimatedHours}',
//     'SET t.doneRatio = {doneRatio}',
//     'SET t.startDate = {startDate}',
//     'SET t.dueDate = {dueDate}',
//     'RETURN t'
//   ];
//   var params = {
//     id: Number(id),
//     subject: String(task.subject),
//     description: String(task.description),
//     estimatedHours: Number(task.estimatedHours),
//     doneRatio: Number(task.doneRatio),
//     startDate: Number(task.startDate),
//     dueDate: Number(task.dueDate)
//   };
//   return neodb.cypherAsync({
//     query: query.join('\n'),
//     params: params
//   });
// }
//
// function updateFeature(id, featureId)
// {
//     var query = [
//       'MATCH (t:Task)',
//       'MATCH (f:Feature)',
//       'MATCH (t)-[r:Feature]->(f)',
//       'WHERE id(t)={id}',
//       'DELETE r',
//       'WITH t',
//       'MATCH (f:Feature)',
//       'WHERE id(f)={featureId}',
//       'MERGE (t)-[:Feature]->(f)',
//       'RETURN t,f'
//     ];
//     var params = {
//       id: Number(id),
//       featureId: Number(featureId)
//     };
//     return neodb.cypherAsync({
//       query: query.join('\n'),
//       params: params
//     });
// }
// function updateUser(id, userId) {
//   var query = [
//     'MATCH (t:Task)',
//     'MATCH (u:User)',
//     'MATCH (t)-[r:Author]->(u)',
//     'WHERE id(t)={id}',
//     'DELETE r',
//     'WITH t',
//     'MATCH (uu:User)',
//     'WHERE id(uu)={userId}',
//     'MERGE (t)-[:Author]->(uu)',
//     'RETURN t,uu'
//   ];
//   var params = {
//     id: Number(id),
//     userId: Number(userId)
//   };
//   return neodb.cypherAsync({
//     query: query.join('\n'),
//     params: params
//   });
// }
//
// function updateStatus(id, status) {
//   var query = [
//     'MATCH (t:Task)',
//     'MATCH (s:taskStatus)',
//     'MATCH (t)-[r:taskStatus]->(s)',
//     'WHERE id(t)={id}',
//     'DELETE r',
//     'WITH t',
//     'MATCH (ss:taskStatus)',
//     'WHERE ss.status = {status}',
//     'MERGE (t)-[:taskStatus]->(ss)',
//     'RETURN t,ss'
//   ];
//   var params = {
//     id: Number(id),
//     status: String(status)
//   };
//   return neodb.cypherAsync({
//     query: query.join('\n'),
//     params: params
//   });
// }
//
// function updatePriority(id, priority) {
//   var query = [
//     'MATCH (t:Task)',
//     'MATCH (p:taskPriority)',
//     'MATCH (t)-[r:taskPriority]->(p)',
//     'WHERE id(t)={id}',
//     'DELETE r',
//     'WITH t',
//     'MATCH (p:taskPriority)',
//     'WHERE p.priority = {priority}',
//     'MERGE (t)-[:taskPriority]->(p)',
//     'RETURN t,p'
//   ];
//   var params = {
//     id: Number(id),
//     priority: String(priority)
//   };
//   return neodb.cypherAsync({
//     query: query.join('\n'),
//     params: params
//   });
// }
//
// function updateTracker(id, tracker) {
//   var query = [
//     'MATCH (t:Task)',
//     'MATCH (tt:taskTracker)',
//     'MATCH (t)-[r:taskTracker]->(tt)',
//     'WHERE id(t)={id}',
//     'DELETE r',
//     'WITH t',
//     'MATCH (tt:taskTracker)',
//     'WHERE tt.tracker = {tracker}',
//     'MERGE (t)-[:taskTracker]->(tt)',
//     'RETURN t,tt'
//   ];
//   var params = {
//     id: Number(id),
//     tracker: String(tracker)
//   };
//   return neodb.cypherAsync({
//     query: query.join('\n'),
//     params: params
//   });
// }
//
// function removeCategory(id) {
//   var query = ['MATCH (t:Task)',
//     'MATCH (c:taskCategory)',
//     'MATCH (t)-[r:taskCategory]->(c)',
//     'WHERE id(t)={id}',
//     'DELETE r'
//   ];
//   var params = {
//     id: Number(id)
//   };
//   return neodb.cypherAsync({
//     query: query.join('\n'),
//     params: params
//   });
//
// function addCategories(id, category) {
//   var makeCat = function() {
//     var result = '[';
//     for (var i = 0; i < category.length; i++) {
//       result += '"' + category[i] + '"';
//       if (i + 1 < category.length) {
//         result += ',';
//       }
//     }
//     return result + ']';
//     };
//   var query = [
//     'MATCH (t:Task) WHERE id(t)=' + id,
//     'MATCH (c:taskCategory) WHERE c.category IN ' + makeCat(),
//     'MERGE (t)-[:taskCategory]->(c)'
//   ];
//   var params = {};
//   return neodb.cypherAsync({
//     query: query.join('\n'),
//     params: params
//   });
// }
