'use strict';
//neo4j
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var graphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new graphDatabase('http://neo4j:neopass@localhost:7474');
var bodyParser = require('body-parser');

module.exports = {
  createTask: createTask,
  getMatchingList: getMatchingList,
  getMatchingListAmount: getMatchingListAmount,
  getCategory: getCategory,
  getStatus: getStatus,
  getPriority: getPriority,
  getTracker: getTracker,
  getPageList: getPageList,
  deleteTask: deleteTask,
  getTaskFromProject: getTaskFromProject,
  getTaskFromFeature: getTaskFromFeature,
  getTaskById: getTaskById,
  addCategories: addCategories
};

function createTask(feature, subject, description, tracker, status, priority, author, category, startDate, dueDate, doneRatio, estimatedHours) {
  var query = [
    'CREATE (t:Task{subject:{subject}, description:{description}, estimatedHours:{estimatedHours}, startDate:{startDate}, dueDate:{dueDate}, doneRatio:{doneRatio}})',
    'WITH t',
    'MATCH (f:Feature),(u:User),(ts:taskStatus),(tp:taskPriority),(tt:taskTracker)',
    'WHERE id(f)={feature}',
    'WITH t,f,u,ts,tt,tp',
    'WHERE id(u)={author}',
    'WITH t,f,u,ts,tt,tp',
    'WHERE tp.priority = {priority}',
    'WITH t,f,u,ts,tt,tp',
    'WHERE tt.tracker = {tracker}',
    'WITH t,f,u,ts,tt,tp',
    'WHERE ts.status = {status}',
    'WITH t,f,u,ts,tt,tp',
    'MERGE (t)-[:Feature]->(f)',
    'MERGE (t)-[:taskAuthor]->(u)',
    'MERGE (t)-[:taskStatus]->(ts)',
    'MERGE (t)-[:taskPriority]->(tp)',
    'MERGE (t)-[:taskTracker]->(tt)',
    'RETURN id(t) as idt'
  ];
  var params = {
    feature: Number(feature),
    subject: String(subject),
    tracker: String(tracker),
    description: String(description),
    status: String(status),
    priority: String(priority),
    author: Number(author),
    category: category,
    estimatedHours: Number(estimatedHours),
    startDate: Number(startDate),
    dueDate: Number(dueDate),
    doneRatio: Number(doneRatio)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}


function getMatchingList(substring, page, pagination) {
     var query = getMatchingQuery(substring);
     var restOfQuery=[
         'RETURN t AS task, f.subject AS featuresubject, id(f) as featureid, u.name AS username, id(u) AS userid, ts.status AS status, tt.tracker AS tracker, tp.priority AS priority, collect(tc.category) as category',
    'SKIP {page}*{pagination}',
    'LIMIT {pagination}'
  ];
  query.push.apply(query, restOfQuery);
  var params = {
    substring: String(substring),
    page: Number(page),
    pagination: Number(pagination)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}
function getMatchingListAmount(substring, page, pagination) {
    var query = getMatchingQuery(substring);
    query.push('WITH t, collect(tc) as col','RETURN count(t) as pagetotal');
    var params = {
      substring: String(substring),
      page: Number(page),
      pagination: Number(pagination)
    };
    return neodb.cypherAsync({
      query: query.join('\n'),
      params: params
    });
}

function getCategory() {
  var query = [
    'MATCH (c:taskCategory)',
    'RETURN collect(c.category) AS category'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getStatus() {
  var query = [
    'MATCH (s:taskStatus)',
    'RETURN collect(s.status) AS status'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getTracker() {
  var query = [
    'MATCH (t:taskTracker)',
    'RETURN collect(t.tracker) AS tracker'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getPriority() {
  var query = [
    'MATCH (p:taskPriority)',
    'RETURN collect(p.priority) AS priority'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getPageList() {
  var query = ['MATCH (t:Task)', 'RETURN COUNT(t) AS AMOUNT'];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function deleteTask(id) {
  var query = [
    'MATCH (t:Task)',
    'WHERE id(t)={id}',
    'DETACH DELETE t'
  ];
  var params = {
    id: Number(id)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getTaskFromProject(projectId) {
  var query = [
    'MATCH (t:Task),(p:Project),(f:Feature)',
    'WHERE id(p)=' + projectId,
    'MATCH (t)-[]->(f)-[]->(p)',
    'RETURN id(t) as idt, t.subject as subject'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getTaskFromFeature(featureId) {
    var query = [
        'MATCH (t:Task),(f:Feature)',
        'WHERE id(f) = ' + featureId,
        'MATCH (f)-[]-(t)',
        'RETURN id(t) as idt, t.subject as subject'
    ];
    var params = {};
    return neodb.cypherAsync({
      query: query.join('\n'),
      params: params
    });
}

function getTaskById(id) {
  var mat = 'MATCH (t:Task)-[]->';
  var query = [
    mat + '(f:Feature)',
    mat + '(s:taskStatus)',
    mat + '(a:taskPriority)',
    mat + '(u:User)',
    mat + '(tt:taskTracker)',
    'WHERE id(t)={id}',
    'OPTIONAL ' + mat + '(c:taskCategory)',
    'RETURN t as task,id(f) as feature, s.status as status, a.priority as priority, tt.tracker as tracker, id(u) as user,collect(c.category) as category'
  ];
  var params = {
    id: Number(id)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function addCategories(id, category) {
  var makeCat = function() {
    var result = '[';
    for (var i = 0; i < category.length; i++) {
      result += '"' + category[i] + '"';
      if (i + 1 < category.length) {
        result += ',';
      }
    }
    return result + ']';
    };
  var query = [
    'MATCH (t:Task) WHERE id(t)=' + id,
    'MATCH (c:taskCategory) WHERE c.category IN ' + makeCat(),
    'MERGE (t)-[:taskCategory]->(c)'
  ];
  var params = {};
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}
function getMatchingQuery(substring) {
    var con = ' CONTAINS "' + substring + '" OR ';
    var mat = 'MATCH (t:Task)-[]->';
    return [
      mat + '(f:Feature)',
      mat + '(u:User)',
      mat + '(tp:taskPriority)',
      mat + '(tt:taskTracker)',
      mat + '(ts:taskStatus)',
      'OPTIONAL ' + mat + '(tc:taskCategory)',
      'WITH t,f,u,tp,tt,tc,ts',
      'WHERE t.subject' + con + 't.description' + con + 'f.subject' + con + 'f.description' + con + 'u.name' + con + 'tp.priority' + con + 'tt.tracker' + con + 'ts.status' + ' contains "' + substring + '"'
  ];
}
