'use strict';
// neo4j
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var graphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new graphDatabase('http://neo4j:neopass@localhost:7474');
var _ = require('lodash');

module.exports = {
  getUserNames: getUserNames,
  getUserListNames: getUserListNames,
  getUserListAmount: getUserListAmount,
  getUserByNick: getUserByNick,
  getUserBy: getUserBy,
  getUserById: getUserById,
  createUser: createUser,
  changeLevel: changeLevel,
  getPermissions: getPermissions,
  getUsers: getUsers
};

function getUserNames(substring) {
  var query = ['MATCH (u:User)',
    'WHERE u.name =~ ".*' + substring + '.*"',
    'RETURN u.name AS name, id(u) AS id'
  ];
  var params = {
    substring: String(substring)
  };
  return neodb.cypherAsync({
    query: query.join('\n'),
    params: params
  });
}

function getUserListQuery(substring) {
  return ['MATCH (u:User)', 'WHERE u.name contains "' + substring + '"'];
}

function getUserListNames(substring, page) {
  var query = getUserListQuery(substring);
  query.push('RETURN u.name AS name, id(u) AS id', 'SKIP ' + page + '*4', 'LIMIT 4');
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function getUserListAmount(substring) {
  var query = getUserListQuery(substring);
  query.push('RETURN count(u) AS amount');
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function getUserByNick(login, pass) {
  var query = [
    'MATCH (u:User) WHERE u.login = "' + login + '" AND WHERE u.pass = "' + pass + '"',
    'RETURN u'
  ];
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function getUserBy(label, value) {
  var query = [
    'MATCH (u:User)-[:Level]->(l:Level) WHERE u.' + label + '="' + value + '"',
    'RETURN u,collect(l.name) as levels'
  ];
  return neodb.cypherAsync({
    query: query.join('\n')
  }).then(function(response) {
    var result = 0;
    if (response.length) {
      result = _.clone(response[0].u.properties);
      result.id = response[0].u._id;
      result.lvl = response[0].levels;
    }
    return result;
  });
}

function getUserById(id) {
  var query = [
    'MATCH (u:User)-[:Level]->(l:Level) WHERE id(u) = ' + Number(id),
    'RETURN u,collect(l.name) as levels'
  ];
  return neodb.cypherAsync({
    query: query.join('\n')
  }).then(function(response) {
    var result = null;
    if (response.length) {
      result = _.clone(response[0].u.properties);
      result.id = response[0].u._id;
      result.lvl = response[0].levels;
    }
    return result;
  });
}

// function createUser(name, email, password){
//     var query = [
//         'CREATE (u:User{name:"'+name+'",email:"'+email+'",password:"'+password+'"})',
//         'RETURN u'
//     ];
//     return neodb.cypherAsync({
//       query: query.join('\n')
//   });
// }

function createUser(name, email, password) {
  var query = [
    'OPTIONAL MATCH (u:User) WHERE u.name="' + name + '" OR u.email="' + email + '"',
    'WITH CASE u IS NULL',
    'WHEN true THEN ["toCreate"]',
    'ELSE []',
    'END as tab',
    'MATCH (level:Level) WHERE level.level=1',
    'FOREACH(toCreate IN tab | CREATE (u:User{name:"' + name + '",email:"' + email + '",password:"' + password + '"})-[:Level]->(level))',
    'RETURN tab'
  ];
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function getPermissions() {
  var query = [
    'MATCH (l:Level)',
    'RETURN collect(l.level) as levels'
  ];
  return neodb.cypherAsync({
    query: query.join('\n')
  });
}

function changeLevel(id, permissions) {
  var query = [
    'MATCH (u:User)-[r:Level]-() WHERE id(u)=' + id,
    'DELETE r',
    'WITH u',
    'MATCH (l:Level) WHERE l.name in [' + permissions.map(function(perm){return '"'+perm+'"'}).join(',') + ']',
    'MERGE (u)-[:Level]->(l)'
  ];
  return neodb.cypherAsync({
    query: query.join('\n')
  }).then(function(response){
      return 'okok';
  });
}

function getUsers() {
  var query = [
    'MATCH (u:User)-[:Level]-(l:Level)',
    'RETURN u, collect(l.name) as levels',
  ];
  return neodb.cypherAsync({
    query: query.join('\n')
  }).then(function(response){
    var result = null;
    if (response.length) {
      result=[];
      response.map(function(resp){
        result.push({
          name: resp.u.properties.name,
          email: resp.u.properties.email,
          lvl: resp.levels,
          id: resp.u._id
        });
      })
    }
    return result;
  });
}
