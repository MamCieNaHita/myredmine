//jshint node: true
'use strict';

var express = require('express');
var controller = require('./../controllers/users.controller');

var usersRouter = express.Router();

usersRouter.get(/^\/usernamelist\/(\w+)$/, controller.getUserNames);
usersRouter.get(/usernamelist/, controller.getUserNames);
usersRouter.get(/^\/usernewlist\/([A-Za-z0-9%]+)\/([0-9]+)$/, controller.getUserNameList);
usersRouter.get(/^\/usernewlist\/\/([0-9]+)$/, controller.getUserNameList);
usersRouter.post('/getuserby', controller.getUserBy);
// usersRouter.post('/auth/register', controller.createUser);
usersRouter.get('/getusers', controller.getUsers);
usersRouter.post('/changelevel', controller.changeLevel);

module.exports = usersRouter;
