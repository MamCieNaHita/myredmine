'use strict';
//jshint node: true
var userService = require('./../services/users.service');

module.exports = {
  getUserNames: getUserNames,
  getUserNameList: getUserNameList,
  getUserBy: getUserBy,
  getUsers: getUsers,
  changeLevel: changeLevel
};

function getUserNames(req, res, next) {
  var substring;
  if (req.params[0] === undefined) {
    substring = '';
  } else {
    substring = req.params[0];
  }
  userService.getUserNames(substring).then(function(data) {
    res.send(data);
  });
}

function getUserNameList(req, res, next) {
  var substring;
  if (req.params[0] === undefined) {
    substring = '';
  } else {
    substring = req.params[0];
  }
  var page = req.params[1];
  if (req.params[1] === undefined) {
    substring = '';
    page = req.params[0];
  }
  userService.getUserListNames(substring, page).then(function(data) {
    userService.getUserListAmount(substring).then(function(amount) {
      res.send(data.map(function(x) {
        x.amount = amount[0].amount;
        return x;
      }));
    });
  });
}

function getUserBy(req, res, next) {
  userService.getUserBy(req.body.property, req.body.value).then(function(response) {
    res.json(response);
  }).catch(function(err) {
    res.json({
      success: false,
      error: "error"
    });
  });
}

function getUsers(req, res, next) {
  if (req.user.lvl.indexOf('admin') !== -1) {
    userService.getUsers().then(function(response) {
      res.json(response);
    }).catch(function(err) {
      res.json({
        success: false,
        error: "error"
      })
    })
  } else {
    res.json({
      success: false,
      error: "no perrmissions"
    })
  }
}

function changeLevel(req, res, next) {
  if (req.user.lvl.indexOf('admin') !== -1) {
    if (!!req.body.id) {
      userService.changeLevel(req.body.id, req.body.levels).then(function(response) {
        res.json({
          success: true,
          message: 'ok'
        });
      }).catch(function(err) {
          res.json({
            success: false,
            error: "error"
          })
        });
      }
      else {
        res.json({
          success: false,
          error: "wrong data"
        })
      }
    } else {
      res.json({
        success: false,
        error: "no perrmissions"
      })
    }
  }
