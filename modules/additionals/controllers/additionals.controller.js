'use strict';
//jshint node: true
var additionalsService = require('./../services/additionals.service');
var Promise = require('bluebird');

module.exports = {
  getExisting: getExisting,
  updateOption: updateOption,
  createOption: createOption,
  deleteOption: deleteOption
};

function getExisting(req, res, next) {
  var types = getTypesAndLabels();
  var promiseArray = [];
  var resultTab = [];
  promiseArray = types.map(function(x) {
    return getBoth(x);
  });
  Promise.each(promiseArray, function(result) {
    resultTab.push(result);
  }).then(function() {
    res.send(resultTab);
  }).catch(function(err){
    res.json({
      success: false,
      error: 'Problem with database'
    });
    console.log('Problem with database - additionals')
  });
}

function getBoth(element) {
  return new Promise(function(fulfill, reject) {
    additionalsService.getWithRelationships(element.type, element.label).then(function(dataWith) {
      additionalsService.getWithoutRelationships(element.type, element.label).then(function(dataWithout) {
        var result = {};
        result = {
          data: dataWith.concat(dataWithout),
          label: element.label,
          type: element.type,
          frontlabel: element.frontlabel
        };
        fulfill(result);
      });
    });
  });
}

function getTypesAndLabels() {
  var tab = [];
  var types = ['Status', 'Priority', 'Category', 'taskStatus', 'taskPriority', 'taskCategory', 'taskTracker'];
  var labels = ['status', 'priority', 'category', 'status', 'priority', 'category', 'tracker'];
  var frontlabels = ['Feature Status', 'Feature Priority', 'Feature Category', 'Task Status', 'Task Priority', 'Task Category', 'Task Tracker'];
  for (var i = 0; i < types.length && i < labels.length; i++) {
    tab.push({
      type: types[i],
      label: labels[i],
      frontlabel: frontlabels[i]
    });
  }
  return tab;
}

function updateOption(req, res, next) {
  var id = req.params[0];
  if (!!req.body.data && !!req.body.label && !!req.body.type) {
    additionalsService.updateOption(id, req.body.type, req.body.label, req.body.data).then(function(data) {
      res.json({response:'ok'});
    }).catch(function(err){
      res.json({response:'error'});
    });
  } else {
    console.log('sth went wrong with updating option');
    res.send('sth went wrong with updating option');
  }
}

function deleteOption(req, res, next) {
  var id = req.params[0];
  additionalsService.deleteOption(id).then(function(data) {
    res.send('');
  }).catch(function(err){
    res.send('error');
  });
}

function createOption(req, res, next) {
  checkIfAlreadyExist(req.body.data, req.body.type).then(function(existing) {
    if (!!req.body.data && !!req.body.label && !!req.body.type && !existing) {
      additionalsService.createOption(req.body.type, req.body.label, req.body.data).then(function(data) {
        res.send(data[0]);
      }).catch(function(err){
        res.send('error');
      });
    } else {
      console.log('sth went wrong with creating option');
      res.send('sth went wrong with creating option');
    }
  })
}

function checkIfAlreadyExist(_label, type) {
  return new Promise(function(fulfill, reject) {

    var types = getTypesAndLabels();
    var promiseArray = [];
    var resultTab = [];
    promiseArray = types.map(function(x) {
      return getBoth(x);
    });
    Promise.each(promiseArray, function(result) {
      resultTab.push(result);
    }).then(function() {
      var check = false;
      resultTab.forEach(function(element) {
        if (element.type === type) {
          element.data.forEach(function(data) {

            if (data.label === _label) {
              check = true;
            }
          });
        }
      });
      fulfill(check);
    }).catch(function(err){
      console.log(err);
    });
  });
}
