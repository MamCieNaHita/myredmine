//jshint node: true
'use strict';

var express = require('express');
var controller = require('./../controllers/additionals.controller');

var additionalsRouter = express.Router();

additionalsRouter.get('/additionals/', controller.getExisting);
additionalsRouter.put(/^\/additionals\/([0-9_]+)$/, controller.updateOption);
additionalsRouter.delete(/^\/additionals\/([0-9_]+)$/, controller.deleteOption);
additionalsRouter.post('/additionals/', controller.createOption);

module.exports = additionalsRouter;
