'use strict';
// neo4j
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var graphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new graphDatabase('http://neo4j:neopass@localhost:7474');
var bodyParser = require('body-parser');

module.exports = {
  getWithRelationships: getWithRelationships,
  getWithoutRelationships: getWithoutRelationships,
  updateOption: updateOption,
  deleteOption: deleteOption,
  createOption: createOption,
};

function getWithRelationships(type, label) {
  var query = [
    'MATCH ()-[r]->(x:' + type + ')', 'RETURN count(r) as amount, x.' + label + ' as label, id(x) as idx ORDER BY id(x)'
  ];
  return neodb.cypherAsync({
    query: query.join('\n'),
  });
}

function getWithoutRelationships(type, label) {
  var query = [
    'MATCH (x:' + type + ')', 'WHERE not ()-[]->(x)', 'RETURN 0 as amount, x.' + label + ' as label, id(x) as idx ORDER BY id(x)'
  ];
  return neodb.cypherAsync({
    query: query.join('\n'),
  });
}

function updateOption(id, type, label, data) {
    var query = [
        'MATCH (x:' + type + ')', 'WHERE id(x)=' + id, 'SET x.' + label + '="' + data + '"'
    ];
    return neodb.cypherAsync({
      query: query.join('\n'),
    });
}

function deleteOption(id) {
    var query = [
        'MATCH (x) WHERE id(x)=' + id + ' WITH x WHERE NOT ()-[]->(x)', 'DETACH DELETE x'
    ];
    return neodb.cypherAsync({
      query: query.join('\n'),
    });
}
function createOption(type, label, data) {
    var query = [
        'CREATE (x:'+ type +'{'+ label + ':"'+ data + '"})','RETURN x'
    ];
    return neodb.cypherAsync({
      query: query.join('\n'),
    });
}
