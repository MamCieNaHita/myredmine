var passport = require('passport');
var userService = require('../../users/services/users.service');
var Promise = require('bluebird');

module.exports = {
  login: login,
  getLoggedUser: getLoggedUser,
  logout: logout,
  createUser: createUser
}

function login(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.json({
        success: false,
        error: "Authentication failed",
        message: info.message
      });
    }
    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }
      return res.json({
        name: user.name,
        email: user.email,
        id: user.id,
        lvl: user.lvl
      });
    });
  })(req, res, next);
}

function getLoggedUser(req, res, next) {
  if (req.isAuthenticated()) {
    userService.getUserById(req.user.id).then(function(user) {
      if (user) {
        res.json({
          success: true,
          user: {
            id: user.id,
            email: user.email,
            name: user.name,
            lvl: user.lvl
          }
        });
      } else {
        throw 'error';
      }
    }).catch(function(err) {
      res.json({
        success: false,
        error: "Problem with database"
      });
    });
  } else {
    res.json({
      success: false,
      error: "Authentication failed"
    });
  }
}

function logout(req, res, next) {
  req.logout();
  res.json({
    success: true,
    message: 'User is successfully logout'
  });
}

function createUser(req, res, next) {
    if(!req.body.name || !req.body.email || !req.body.password){
        res.json({
          success: false,
          error: 'Problem with data'
        })
        return;
    }
    userService.createUser(req.body.name, req.body.email, req.body.password).then(function(response){
        if(response[0].tab.length){
            res.json({
                success: true
            })
        }
        else {
            res.json({
                success: false,
                error: 'User already exists'
            })
        }
    }).catch(function(err){
        res.json({
          success: false,
          error: 'Problem with database'
        })
    })
}
