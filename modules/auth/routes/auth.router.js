// var express = require('express');
// var authRouter = express.Router();
// var authController = require('./../controllers/auth.controller');

var express = require('express');
var authController = require('./../controllers/auth.controller');
var projectController = require('../../project/controllers/project.controller');
var featureController = require('../../features/controllers/features.controller');
var taskController = require('../../tasks/controllers/tasks.controller');



var authRouter = express.Router();


authRouter.post('/auth/login', authController.login);
authRouter.get('/auth/getuser', authController.getLoggedUser);
authRouter.get('/auth/logout', authController.logout);
authRouter.post('/auth/register', authController.createUser);
authRouter.get(/^\/projectamount$/, projectController.getProjectAmount);


module.exports = authRouter;
