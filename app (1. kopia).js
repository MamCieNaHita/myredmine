'use strict';
// NEO4J
//jshint node: true
var neo4j = require('neo4j');
var Promise = require('bluebird');
var graphDatabase = Promise.promisifyAll(neo4j).GraphDatabase;
var neodb = new graphDatabase('http://neo4j:neopass@localhost:7474');
var express = require('express');
var app=express();
var router=express.Router();
var bodyParser = require('body-parser');
//console.log(neodb);
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE');
  next();
});

function getNode(id){
	var query = [
		/*'START n=NODE({id})',
		'MATCH (n)-[r]->(n2)',
		'RETURN n,r,n2'*/
		'MATCH (m:Project)',
		'WHERE id(m) = {id}',
		'RETURN m'];

	var params = {
		id: Number(id)
	};

	return neodb.cypherAsync({
		query : query.join('\n'),
		params: params
	});
}


router.get(/^\/project\/([0-9_]+)$/, function(req, res, next) {
 var id = req.params[0];
 getNode(id).then(function(data){
  res.send(data[0]);
 });
});


function createNode(name, identifier, description) {
	var query = ['CREATE (:Project{name:{name}, identifier:{identifier}, description:{description}}) '];

	var params = {
		name: String(name),
		identifier: String(identifier),
		description: String(description)
	};
	console.log('WTFWTFWTF');

	return neodb.cypherAsync({
	query : query.join('\n'),
	params: params
})}

router.post('/project/', function(req, res, next) {
	//var id = req.params[0];
	console.log(req.body);
	createNode(req.body.name, req.body.identifier, req.body.description).then(function(data){
		res.send(data[0]);
		console.log(data);
	});
	});

function getList() {
	var query =['MATCH (m:Project)', 'return m'];
	var params = {};
	return neodb.cypherAsync({
		query : query.join('\n'),
		params: params
	});
}

router.get('/project/', function(req, res, next) {
	getList().then(function(data){
		res.send(data);
	});
})
function deleteNode(id) {
	var query = [
		'MATCH (m:Project)',
		'WHERE id(m) = {id}',
		'WITH m',
		'DETACH DELETE m'];
	var params = {
		id: Number(id)
	};
		return neodb.cypherAsync({
		query : query.join('\n'),
		params: params
	});
}

router.delete(/^\/project\/([0-9_]+)$/, function(req, res, next) {
	var id = req.params[0];
	deleteNode(id).then(function(data){
		res.send(data);
	});
})

function updateNode(id, name, identifier, description) {
	var query = [
		'MATCH (m:Project)',
		'WHERE id(m) = {id}',
		'WITH m',
		'SET m.name = {name}',
		'SET m.identifier = {identifier}',
		'SET m.description = {description}',
		'RETURN m'];
	var params = {
		id: Number(id),
		name: String(name),
		identifier: String(identifier),
		description: String(description)
	};
	return neodb.cypherAsync({
		query : query.join('\n'),
		params: params
	});
}

router.put(/^\/project\/([0-9_]+)$/, function(req, res, next) {
	var id = req.params[0];
	updateNode(id, req.body.name, req.body.identifier, req.body.description).then(function(data){
		res.send(data[0]);
	});
})


app.use('/',router);

getNode(1).then(function(result){
	// do something with result
	console.log(result);
});

app.listen(5000, function () {
  console.log('Example app listening on port 5000!');
})
