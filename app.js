'use strict';
//jshint node: true
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var projectRouter = require('./modules/project/routes/project.router');
var featureRouter = require('./modules/features/routes/features.router');
var usersRouter = require('./modules/users/routes/users.router');
var tasksRouter = require('./modules/tasks/routes/tasks.router');
var additionalsRouter = require('./modules/additionals/routes/additionals.router');
var universalRouter = require('./modules/universals/routes/universal.router');
var authRouter = require('./modules/auth/routes/auth.router');
var passportService = require('./modules/auth/services/auth.service');
var passport = require('passport');
var cookieParser = require('cookie-parser');
var session = require('express-session');

// app.use(express.static(__dirname+'/src'));
// app.use(express.static(__dirname+'/bower_components'));
// app.use(express.static(__dirname+'/dist'));


app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({ secret: 'keyboard cat', resave: false, saveUninitialized: true, maxAge: 100000000 }));
app.use(passport.initialize());
app.use(passport.session());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Credentials', true);
  next();
});
passportService.initPassport();

app.use(authRouter);
app.use(function(req,res,next){
    if(req.isAuthenticated()){
        next();
    }
    else{
        res.json({
            success: false,
            error: "Not authenticated"
        }).status(403);
    }
});
app.use(projectRouter);
app.use(featureRouter);
app.use(usersRouter);
app.use(tasksRouter);
app.use(additionalsRouter);
app.use(universalRouter);

app.listen(5000);

console.log('SERWER DZIALA');
